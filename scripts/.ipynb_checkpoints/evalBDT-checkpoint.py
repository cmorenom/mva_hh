
#!/usr/bin/env python

import argparse
import logging
import os
import re
import resource

import h5py as h5
import numpy as np

import xgboost as xgb

import mbj_nn.dataset
import mbj_nn.utils

def _get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True)
    args.add_argument('--branches', metavar='branches.list', required=True)
    args.add_argument('--trained_model', metavar='test_BDT.json', required=True)
    args.add_argument('--groups-sig', nargs='+', default=['Gtt', 'shh'])
    args.add_argument('--parameters')

    return args.parse_args()

def create_structured(datax, mass):
    nentries = datax.shape[0]
    noutput = 1
    dtype = []
    dtype.append((
        'BDToutput_{}'.format(int(mass)),
        'f4',
        (noutput,)
    ))

    return np.zeros((nentries,), dtype=dtype)



# TO-DO: Add function to gess mass

def create_output(inpath, outpath, branches, keys, BDT, mass):
    logging.info("Evaluating %s with output to %s", inpath, outpath)
    with h5.File(outpath, 'x') as ofile:
        it = mbj_nn.dataset.itertrees(
            inpath,
            branches,
            keys=keys,
            allowed=None,
            nominal_only=False
        )
        for treename, datax in it:
            logging.info("tree=%s", treename)
            output = create_structured(datax, mass)
            # log_rss()

            okey = 'BDToutput_{}'.format(int(mass))
            if datax.shape[0] > 0:
                data_x = xgb.DMatrix(datax)
                print(datax.shape, data_x.num_row(), data_x.num_col())
                prediction = BDT.predict(data_x)
                output[okey] = np.reshape(prediction, (prediction.size, 1))
                # log_rss()

            ofile.create_dataset(
                treename.split(';')[0],
                data=output,
                compression='gzip'
            )


def _main():
    args = _get_args()

    bdt = xgb.Booster()
    bdt.load_model(args.trained_model)

    branches = mbj_nn.dataset.read_branches(args.branches)
    
    for path in args.files:
        suf = '.BDToutput.h5'
        opath = os.path.basename(path) + suf

        keys = []
        
        create_output(
            inpath = path,
            outpath = opath,
            branches = branches,
            keys = keys,
            BDT = bdt,
            mass = 500
        )
if __name__ == '__main__':
 
    print("Hello world")
    logging.basicConfig(
        level='INFO',
        format = '[%(asctime)s %(filename)s %(levelname)s] %(message)s'
    )
    _main()
