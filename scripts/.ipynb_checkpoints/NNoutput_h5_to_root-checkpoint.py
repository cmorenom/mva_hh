#!/usr/bin/env python

import argparse
import logging
import os

import h5py as h5
import ROOT
import root_numpy

from mbj_nn import utils

def _convert(outp, friend=None):
    with h5.File(outp, 'r') as ifile:
        outpath = os.path.basename(outp).replace('.h5', '.root')
        if not outpath.endswith('.root'):
            outpath += '.root'
        logging.info('%s -> %s', outp, outpath)
        tf = ROOT.TFile.Open(outpath, "CREATE")
        tf.cd()
        for key in ifile.keys():
            logging.info('tree: %s', key)
            tree = root_numpy.array2tree(
                ifile[key][()],
                name=key
            )
            if friend:
                tree.AddFriend(key, friend)
            tree.Write("", ROOT.TObject.kWriteDelete)
        tf.Close()



def _main():

    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True)
    args.add_argument('--no-friends', action='store_true')
    args.add_argument('--group-sig', default=None)
    args = args.parse_args()

    if args.no_friends:
        for path in utils.expand_files(args.files):
            _convert(path)
    else:
        files_in, files_out = utils.group_files(
            utils.expand_files(args.files),
            group_sig=args.group_sig
        )
        #for fold in ['training', 'validation', 'test', 'data']:
        for fold in ['training', 'validation', 'test', 'data', 'all']:
            logging.info('%s', fold)
            for inp, outp in zip(files_in[fold], files_out[fold]):
                _convert(outp, friend=inp)


if __name__ == '__main__':
    logging.basicConfig(
        level='INFO',
        format='[%(asctime)s %(filename)s %(levelname)s] %(message)s'
    )
    _main()
