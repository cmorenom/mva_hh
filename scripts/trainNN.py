#!/usr/bin/env python3

import argparse
import glob
import json
import logging
import random

import h5py as h5
import numpy as np
import tensorflow as tf

import mbj_nn.dataset
import mbj_nn.model
import mbj_nn.utils


def _get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True, help='file list, accepts *')
    args.add_argument('--sel', help='json with selections', required=False)
    args.add_argument('--sel-sig', metavar='sel_sig.json', help='json with selections for  signals (e.g. only hh->4b)', required=False)
    args.add_argument('--branches', metavar='branches.list', required=True)
    args.add_argument(
        '--group-to-target',
        metavar='group_to_target.json',
        required=True
    )
    args.add_argument('--model', metavar='model.json', required=True)
    args.add_argument('--restart', nargs=3, help='<restart-weights> <restart-epoch> <restart-val>', default=None)
    args.add_argument('--max-epoch', type=int, default=None)
    args.add_argument('--output', default='trained_model.h5')
    args.add_argument('--seed', default=None, type=int)
    return args.parse_args()


def _main():
    args = _get_args()

    if not args.seed:
        seed = random.SystemRandom().randint(1, 2**31 - 1)
    else:
        seed = args.seed

    logging.info("seed: %s", seed)
    np.random.seed(seed)

    logging.info("Loading the hyperparameters")
    with open(args.model, 'r') as ifile:
        hps = json.load(ifile)

    sel=None
    if args.sel:
        with open(args.sel, 'r') as fsel:
            sel = json.load(fsel)

    sel_sig=None
    if args.sel_sig:
        with open(args.sel_sig, 'r') as fsel_sig:
            sel_sig = json.load(fsel_sig)
    if sel_sig:
        print('Sel sig!')
        print(sel_sig)

    logging.info("Loading the dataset")
    data = mbj_nn.dataset.load(
        mbj_nn.utils.expand_files(args.files), # expands * in args.files
        mbj_nn.dataset.read_branches(args.branches), # branches.strip() as list
        mbj_nn.dataset.read_group_to_target(args.group_to_target), # loads dictionary
        hps=hps,
        sel=sel,
        sel_sig=sel_sig
    )

    logging.info("Setting up model for training")
    nn, fitcfg, norm = mbj_nn.model.setup_training(
        hps=hps,
        data=data,
        output_path=args.output,
        restart=args.restart
    )

    if args.max_epoch:
        logging.warning("Running on a maximum of %d epochs", args.max_epoch)
        fitcfg['epochs'] = args.max_epoch

    logging.info("Starting training")
    hist = nn.fit(**fitcfg)
    logging.info("Training done")

    with h5.File(args.output, 'r+') as mfile:
        logging.info("Saving normalization constants")
        mfile.create_dataset('normalization/mean', data=norm['mean'])
        mfile.create_dataset('normalization/std', data=norm['std'])
        logging.info("Saving training history")
        for key, val in hist.history.items():
            mfile.create_dataset('history/{}'.format(key), data=np.array(val))

    logging.info('All done!')

if __name__ == '__main__':
    logging.basicConfig(
        level='INFO',
        format='[%(asctime)s %(filename)s %(levelname)s] %(message)s'
    )
    _main()
