#!/usr/bin/env python3

import argparse
import glob
import json
import logging
import random

import numpy as np

import mbj_nn.plot_utils

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--files', nargs='+', required=True, help='file list, accepts *')
    parser.add_argument('--sel', help='json with selections', required=False)
    parser.add_argument('--sel-sig', metavar='sel_sig.json', help='json with specific selections for signals', required=False)
    parser.add_argument('--files-test', nargs='+', help='evaluation file list, accepts *')

    args = parser.parse_args()

    sel=None
    if args.sel:
        with open(args.sel, 'r') as fsel:
            sel = json.load(fsel)

    sel_sig=None
    if args.sel_sig:
        with open(args.sel_sig, 'r') as fsel_sig:
            sel_sig = json.load(fsel_sig)

    if sel_sig:
        print('Sel sig!')
        print(sel_sig)

    selection = mbj_nn.plot_utils.selection_to_string(sel)
    selection_w = "("+selection+")*weight_mc*weight_lumi_real*weight_elec*weight_muon*weight_jvt*weight_WZ_2_2*weight_pu*weight_met_trig_3"
    print(selection)

    selection_signal = mbj_nn.plot_utils.selection_to_string(sel_sig)
    selection_signal_w = "("+selection_signal+")*weight_mc*weight_lumi_real*weight_elec*weight_muon*weight_jvt*weight_WZ_2_2*weight_pu*weight_met_trig_3"

    signal_files = [ k for k in args.files if 'GGMH' in k ]
    bkg_files = [ k for k in args.files if 'GGMH' not in k ]

    signal_files_test = [ k for k in args.files_test if 'GGMH' in k ]
    bkg_files_test = [ k for k in args.files_test if 'GGMH' not in k ]

    print(signal_files)
    print(bkg_files)
    print(signal_files_test)
    print(bkg_files_test)

    mbj_nn.plot_utils.plot_NN_output(signal_files, 'GGMH_500_nominal', bkg_files, 'tree', selection_w, 'NNoutput_0_500_0[0]', selection_signal = selection_signal_w, signal_test = signal_files_test, background_test = bkg_files_test, plot_ROC = True)

    mbj_nn.plot_utils.plot_ROC(signal_files, 'GGMH_500_nominal', bkg_files, 'tree', selection_w, 'NNoutput_0_500_0[0]', selection_signal = selection_signal_w, signal_test = signal_files_test, background_test = bkg_files_test, nbins = 200)

