#!/usr/bin/env python

import argparse
import collections
import multiprocessing
import os

import h5py as h5
import numpy as np


def _average(paths):
    for base in paths:
        print('==> ' + base)
        with h5.File(base, 'x') as ofile:
            f = 1.0 / len(paths[base])
            trees = {k.split(';')[0] for k in h5.File(paths[base][0], 'r').keys()}
            for tree in trees:
                print('  -> ' + tree)
                n = h5.File(paths[base][0], 'r')[tree].shape[0]
                dt = h5.File(paths[base][0], 'r')[tree].dtype
                dset = np.zeros((n,), dtype=dt)
                for p in paths[base]:
                    data = h5.File(p, 'r')[tree]
                    for field in data.dtype.names:
                        dset[field] += f * data[field]
                ofile.create_dataset(name=tree, data=dset)


def _wrangle(files):
    d = collections.defaultdict(list)
    for p in files:
        d[os.path.basename(p)].append(p)
    d.default_factory = None
    return d


def _get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True)
    args.add_argument('--njob', type=int, default=1)
    return args.parse_args()


def _main():
    args = _get_args()

    if len(args.files) == 1 and args.files[0].endswith('.list'):
        with open(args.files[0], 'r') as ifile:
            files = [l.strip() for l in ifile.readlines()]
    else:
        files = args.files

    lists = _wrangle(files)

    if args.njob == 1:
        # sequential
        _average(lists)
    else:
        # One distinct output for each key
        # So can parallelize on (key, item)
        pool = multiprocessing.Pool(args.njob)
        pool.map(_average, [{k: v} for k, v in lists.items()])


if __name__ == '__main__':
    _main()
