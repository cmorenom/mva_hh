#!/bin/env python
""" Link NNoutputs with HFinputs """
import argparse
import glob
import multiprocessing
import os
import re

import ROOT


def main():
    args = get_args()

    payloads = []

    if args.NNoutput_dir:
        paths = glob.glob(args.NNoutput_dir + '/*.NNoutput.root')
    else:
        paths = args.NNoutputs

    for path_NNoutput in paths:
        base = os.path.basename(path_NNoutput)

        if re.search(r'\.[0-9]\.NNoutput.root$', base):
            regexp = r'\.[0-9]\.NNoutput.root$'
        else:
            regexp = r'\.root\.NNoutput.root$'

        path_weightNN = '{}/{}'.format(
            args.weight_NN_dir,
            re.sub(regexp, '.weight_NN.root', base)
        )

        path_HFinput = '{}/{}'.format(
            args.HFinput_dir,
            re.sub(r'\.NNoutput\.root$', '', base)
        )

        if args.jobs == 1:
            add_friends(
                path_NNoutput,
                path_weightNN,
                path_HFinput,
                args.remove_existing
            )

        else:
            payloads.append((
                path_NNoutput,
                path_weightNN,
                path_HFinput,
                args.remove_existing
            ))

    if payloads:
        multiprocessing.Pool(args.jobs).map(worker, payloads)


def get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--weight_NN-dir', required=True)
    args.add_argument('--HFinput-dir', required=True)
    args.add_argument('--jobs', type=int, default=1)
    args.add_argument('--remove-existing', action='store_true')

    grp = args.add_mutually_exclusive_group(required=True)
    grp.add_argument('--NNoutput-dir')
    grp.add_argument('--NNoutputs', nargs='+')

    return args.parse_args()


def add_friends(nnoutput, weightNN, hfinput, remove_existing):
    tfile_NNoutput = tfile(nnoutput, mode='UPDATE')
    tfile_weightNN = tfile(weightNN)
    tfile_HFinput = tfile(hfinput)

    trees = {k.GetName() for k in tfile_NNoutput.GetListOfKeys()}

    for treename in trees:
        ttree = tfile_NNoutput.Get(treename)
        friends = ttree.GetListOfFriends()

        if friends:
            if remove_existing:
                for friend in list(friends):
                    ttree.RemoveFriend(friend.GetTree())
            else:
                print 'WARNING: tree {} in {} already has friends! SKIP'.format(
                    treename,
                    nnoutput
                )
                continue

        ttree.AddFriend(treename, tfile_HFinput)
        ttree.AddFriend(treename, tfile_weightNN)

        tfile_NNoutput.cd()
        ttree.Write('', ROOT.TObject.kOverwrite)

    tfile_NNoutput.Close()
    tfile_weightNN.Close()
    tfile_HFinput.Close()


def worker(payload):
    add_friends(*payload)


def tfile(path, mode='READ'):
    if not os.path.exists(path):
        raise RuntimeError("{} not found!".format(path))
    tf = ROOT.TFile.Open(path, mode)
    if tf.IsZombie():
        raise RuntimeError("Unable to open {}!".format(path))
    return tf

if __name__ == '__main__':
    main()
