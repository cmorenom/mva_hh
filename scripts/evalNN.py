#!/usr/bin/env python

import argparse
import logging
import os
import re
import resource

import h5py as h5
import keras
import numpy as np

import mbj_nn.dataset
import mbj_nn.utils


def log_rss():
    vmrss = None
    with open('/proc/{}/status'.format(os.getpid()), 'r') as stats:
        for line in stats.readlines():
            if line.startswith('VmRSS'):
                vmrss = line.strip()
                break
        logging.debug('%s', vmrss)


def _get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True)
    args.add_argument('--branches', metavar='branches.list', required=True)
    args.add_argument('--model', metavar='model.h5', required=True)
    args.add_argument('--groups-sig', nargs='+', default=['Gtt', 'shh'])
    args.add_argument('--no-signal-match', action='store_true')
    args.add_argument('--ncpu', type=int, default=None)
    args.add_argument('--parameters')
    args.add_argument('--trees', nargs='+', default=None)
    args.add_argument('--nominal-only', action='store_true')

    grp = args.add_mutually_exclusive_group()
    grp.add_argument('--do-data', action='store_true')
    grp.add_argument('--bkg-only', action='store_true')

    return args.parse_args()


def create_structured(datax, nn, parameters):
    nentries = datax.shape[0]
    noutput = nn.output_shape[-1]
    dtype = []
    for isGtt, mg, ml in parameters:
        dtype.append((
            'NNoutput_{}_{}_{}'.format(int(isGtt), int(mg), int(ml)),
            'f4',
            (noutput,)
        ))

    return np.zeros((nentries,), dtype=dtype)


def create_output(inpath, outpath, branches, keys, nn, parameters,
                  no_signal_match, normalization, trees, nominal_only):
    mean, std = normalization
    logging.info("Evaluating %s with output to %s", inpath, outpath)
    with h5.File(outpath, 'x') as ofile:
        it = mbj_nn.dataset.itertrees(
            inpath,
            branches,
            keys=keys,
            allowed=trees,
            nominal_only=nominal_only
        )
        for treename, datax in it:
            logging.info("tree=%s", treename)
            datax -= mean
            datax /= std
            output = create_structured(datax, nn, parameters)
            # log_rss()
            for isGtt, mg, ml in parameters:
                if not no_signal_match:
                    # signal matching requested (default)
                    m = re.match('shh_zp([0-9]*)_dm200_dh([0-9]*)', treename)
                    if m:
                        if not (mg == float(m.group(1)) and
                                ml == float(m.group(2))):
                            continue
                        if isGtt == 1 and not treename.startswith('Gtt'):
                            continue
                logging.debug("isGtt=%s, m_gluino=%s, m_lsp=%s", isGtt, mg, ml)
                mbj_nn.dataset.set_parameters(
                    datax,
                    isGtt,
                    mg,
                    ml,
                    normalization=(mean, std)
                )
                okey = 'NNoutput_{}_{}_{}'.format(int(isGtt), int(mg), int(ml))
                if datax.shape[0] > 0:
                    output[okey] = nn.predict(datax, batch_size=1024)
                # log_rss()

            ofile.create_dataset(
                treename.split(';')[0],
                data=output,
                compression='gzip'
            )


def _main():
    args = _get_args()

    if args.ncpu:
        import tensorflow as tf
        config = tf.ConfigProto()
        config.intra_op_parallelism_threads = args.ncpu
        config.inter_op_parallelism_threads = args.ncpu
        keras.backend.set_session(tf.Session(config=config))

    files = mbj_nn.utils.expand_files(args.files)

    logging.info('Loading model from %s', args.model)
    nn = keras.models.load_model(args.model)

    logging.info('Loading normalization constants')
    with h5.File(args.model, 'r') as nnfile:
        mean = nnfile['normalization/mean'][()]
        std = nnfile['normalization/std'][()]

    if args.parameters:
        files_for_params = [args.parameters]
    else:
        files_for_params = [
            p for p in files if os.path.basename(p).startswith('GGMH')
        ]
    if args.do_data:
        files = [
            p for p in files if os.path.basename(p).startswith('Data_')
        ]
    if args.bkg_only:
        files = [
            p for p in files if os.path.basename(p).startswith('Bkg_')
        ]

    branches = mbj_nn.dataset.read_branches(args.branches)
    # log_rss()

    parameters = mbj_nn.utils.get_parameters(files_for_params, groups_sig=args.groups_sig)
    if parameters.shape[0] == 0:
        logging.warning('No parameters!')

    for path in files:
        suf = '.NNoutput.h5'
        if path.endswith('.NNinput'):
            opath = os.path.basename(path).replace('.NNinput', suf)
        else:
            opath = os.path.basename(path) + suf

        keys = []
        if args.do_data:
            keys += ['data']
        if args.trees:
            keys += args.trees

        create_output(
            inpath=path,
            outpath=opath,
            branches=branches,
            keys=keys,
            nn=nn,
            parameters=parameters,
            no_signal_match=args.no_signal_match,
            normalization=(mean, std),
            trees=args.trees,
            nominal_only=args.nominal_only
        )


if __name__ == '__main__':
    logging.basicConfig(
        level='INFO',
        format='[%(asctime)s %(filename)s %(levelname)s] %(message)s'
    )
    _main()
    # log_rss()
