#!/usr/bin/env python

import argparse
import logging
import multiprocessing

import os
import re

import h5py as h5
import numpy as np

import mbj_nn.utils

def compute_significance(data_input, data_output, threshold, group_sig,
                         parameters, groups_bkg, uncert=0.5, index=0):
    """ Compute significance for a signal point """


    variable =  'NNoutput_{}_{}_{}'.format(*parameters)

    s_scores, s_weights = mbj_nn.utils.get_scores_and_weights(
        data_input,
        data_output,
        ['{}_{}_5000_{}_nominal'.format(group_sig, *parameters[1:])],
        variable,
        index
    )

    b_scores, b_weights = mbj_nn.utils.get_scores_and_weights(
        data_input,
        data_output,
        [g + '_nominal' for g in groups_bkg],
        variable,
        index
    )

    return mbj_nn.utils.significance(
        np.sum(s_weights[np.where(s_scores >= threshold)]),
        np.sum(b_weights[np.where(b_scores >= threshold)]),
        uncert
    )


def _worker(payload):
    z = compute_significance(*payload)
    logging.info('mg=%d, ml=%d, Z=%f', payload[4][0], payload[4][1], z)
    return z


def _get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True)
    args.add_argument('--thresholds', required=True)
    args.add_argument('--uncert', type=float, default=0.5)
    args.add_argument('--group-sig', default='Gtt')
    args.add_argument('--group-index', default=0, type=int)
    args.add_argument('--setname', choices=['training', 'validation', 'test'], default='validation')
    args.add_argument('--output', default='significance.h5')
    args.add_argument('--njob', type=int, default=1)
    return args.parse_args()


def _main():
    args = _get_args()
    data_input, data_output = mbj_nn.utils.group_files(
        mbj_nn.utils.expand_files(args.files),
    )

    parameters =  mbj_nn.utils.get_parameters(
        data_input[args.setname],
        args.group_sig,
        dtype=int
    )

    thrs = {}
    with open(args.thresholds, 'r') as thrfile:
        for line in thrfile.readlines():
            fields = line.strip().split()
            thrs[(int(fields[0]), int(fields[1]))] = np.float32(fields[2])

    zs = np.zeros(
        (len(parameters),),
        dtype=[('mg', 'f4'), ('ml', 'f4'), ('z', 'f4')]
    )

    if args.njob == 1:
        # sequential
        for i, (isGtt, mg, ml) in enumerate(parameters):
            logging.info('isGtt=%d, mg=%d, ml=%d', isGtt, mg, ml)
            z = compute_significance(
                data_input[args.setname],
                data_output[args.setname],
                thrs[(mg, ml)],
                args.group_sig,
                (isGtt, mg, ml),
                mbj_nn.utils.get_bkg_names(data_input[args.setname]),
                args.uncert,
                args.group_index,
            )

            zs['mg'][i] = mg
            zs['ml'][i] = ml
            zs['z'][i] = z

            logging.info('Z=%f', z)

    else:
        payloads = []
        for i, (isGtt, mg, ml) in enumerate(parameters):
            payloads.append((
                data_input[args.setname],
                data_output[args.setname],
                thrs[(mg, ml)],
                args.group_sig,
                (isGtt, mg, ml),
                mbj_nn.utils.get_bkg_names(data_input[args.setname]),
                args.uncert,
                args.group_index,
            ))
        pool = multiprocessing.Pool(args.njob)
        zlist = pool.map(_worker, payloads)
        for i, ((_, mg, ml), z) in enumerate(zip(parameters,zlist)):
            zs['mg'][i] = mg
            zs['ml'][i] = ml
            zs['z'][i] = z

    nexcl = np.count_nonzero(zs['z'] >= 1.64)
    logging.info('excluded=%d', nexcl)

    with h5.File(args.output, 'x') as ofile:
        dset = ofile.create_dataset('significance', data=zs)
        dset.attrs['nexcl'] = nexcl
    logging.info('Wrote %s', args.output)




if __name__ == '__main__':
    logging.basicConfig(
        level='INFO',
        format='[%(asctime)s %(filename)s %(levelname)s] %(message)s'
    )
    _main()
