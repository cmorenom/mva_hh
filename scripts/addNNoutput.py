#!/usr/bin/env python

import argparse
import logging

import ROOT

import mbj_nn.utils

def _main():
    args = argparse.ArgumentParser()
    args.add_argument('--NNoutput', nargs='+', required=True)
    args.add_argument('--dest', nargs='+', required=True)
    args = args.parse_args()

    print("hello world")

    l_NNoutput = mbj_nn.utils.expand_files(args.NNoutput)
    l_dest = mbj_nn.utils.expand_files(args.dest)

    for foutput,fdest in zip(l_NNoutput,l_dest):
        print(foutput,fdest)

        source = ROOT.TFile.Open(foutput, 'READ')
        dest = ROOT.TFile.Open(fdest, 'UPDATE')
        
        source_keys = [k.GetName() for k in source.GetListOfKeys()]
        for key in [k.GetName() for k in dest.GetListOfKeys()]:
            if key in source_keys:
                logging.info("Adding friend for TTree=%s", key)
                dest_tree = dest.Get(key)
                dest_tree.AddFriend(key, source)
                dest_tree.Write("", ROOT.TObject.kOverwrite)
            else:
                logging.info("Skipping TTree=%s", key)

if __name__ == '__main__':
    logging.basicConfig(level='INFO')
    _main()
