#!/usr/bin/env python

import argparse
import logging
import multiprocessing
import os
import re

import h5py as h5
import numpy as np
import scipy.special
import sklearn.metrics
import uproot

import mbj_nn.utils


def compute_threshold(data_input, data_output, group_sig, parameters, groups_bkg,
                      uncert=0.5, max_stat_uncert=0.3, index=0, setname='validation',
                      compat=False, bkg_path=None, met=None, total_bkg_uncert=False,
                      mincontrolfract=0.0):

    if not compat:
        variable =  'NNoutput_{}_{}_{}'.format(*parameters)
    else:
        variable =  'NNoutput_{}_{}'.format(*parameters[1:])

    keys = ['{}_{}_5000_{}_nominal'.format(group_sig, *parameters[1:])] \
         + [g + '_nominal' for g in groups_bkg]

    scores, weights, labels = mbj_nn.utils.get_scores_and_weights(
        data_input[setname],
        data_output[setname],
        keys,
        variable,
        index,
        labels=True,
        met=met
    )

    # control data
    if total_bkg_uncert:
        c_sel = labels == 0
        c_scores, c_weights = scores[c_sel], weights[c_sel]
    else:
        c_scores, c_weights = mbj_nn.utils.get_scores_and_weights(
            data_input[setname],
            data_output[setname],
            ['ttbar_nominal'],
            variable,
            index,
            met=met
        )

    return compute_threshold_from_scores_and_weights(
        scores,
        weights,
        labels,
        c_scores,
        c_weights,
        uncert,
        max_stat_uncert,
        bkg_path,
        total_bkg_uncert,
        mincontrolfract
    )


def compute_threshold_from_scores_and_weights(scores,
                                              weights,
                                              labels,
                                              c_scores,
                                              c_weights,
                                              uncert,
                                              max_stat_uncert,
                                              bkg_path,
                                              total_bkg_uncert,
                                              mincontrolfract):

    fpr, tpr, thr = sklearn.metrics.roc_curve(
        y_true=labels,
        y_score=scores,
        sample_weight=weights,
        drop_intermediate=False
    )

    fpr = fpr.astype(np.float64)
    tpr = tpr.astype(np.float64)

    stot = np.sum(weights * labels),
    btot = np.sum(weights * (1 - labels))
    zs = mbj_nn.utils.significance(
        tpr * stot,
        fpr * btot,
        uncert
    )

    # Default: every instances classified as signal
    i_star = 0
    control_uncert = float('inf')

    for i in np.argsort(zs)[::-1]:
        nbkg = fpr[i] * btot
        # require at least 0.5 bkg event
        if nbkg < 0.5:
            continue
        w_sel = c_weights[c_scores >= thr[i]]
        if total_bkg_uncert:
            w_sum = fpr[i] * btot
        else:
            w_sum = np.sum(w_sel)
        if w_sum > 0:
            w_sum_squared = np.sum(w_sel * w_sel)
            control_uncert = np.sqrt(w_sum_squared) / w_sum
            if (w_sum / nbkg) > mincontrolfract and control_uncert <= max_stat_uncert:
                # zs is sorted from highest to lowest
                # So if we get here, this is the best we are going to get
                i_star = i
                break

    thr_star, zs_star = thr[i_star], zs[i_star]

    if bkg_path:
        with h5.File(bkg_path, 'x') as bfile:
            # sum of weights is cached already
            wsum = fpr[i_star] * btot
            # sum of weights squared may be cached
            if total_bkg_uncert:
                # in this case, it's already computed for the control uncert
                wuncert = control_uncert
            else:
                w2sel = weights[(labels == 0)&(scores >= thr_star)]
                w2sum = np.sum(w2sel * w2sel)
                # uncert
                wuncert = np.sqrt(w2sum) / wsum

            bfile.create_dataset('yields', data=np.array([wsum]))
            bfile.create_dataset('yields_uncert', data=np.array([wuncert]))
            bfile.create_dataset('control_uncert', data=np.array([control_uncert]))

        logging.info('Wrote %s', bkg_path)

    return thr_star, zs_star

def _worker(payload):
    thr, zexp = compute_threshold(*payload)
    logging.info(
        'mg=%s, ml=%d: threshold=%f, zexp=%f',
        payload[3][1],
        payload[3][2],
        thr,
        zexp
    )
    return thr, zexp


def _get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True)
    args.add_argument('--uncert', type=float, default=0.5)
    args.add_argument('--max-stat-uncert', type=float, default=0.3)
    args.add_argument('--group-sig', default='Gtt')
    args.add_argument('--group-index', default=0, type=int)
    args.add_argument('--output', default='thresholds.csv')
    args.add_argument('--njob', type=int, default=1)
    args.add_argument('--setname', default='validation')
    args.add_argument('--compat', action='store_true')
    args.add_argument('--parameters', nargs='+')
    args.add_argument('--save-bkg-yields', action='store_true')
    args.add_argument('--met', type=int, default=100)
    args.add_argument('--total-bkg-uncert', action='store_true')
    args.add_argument('--min-control-fract', type=float, default=0.0)
    args.add_argument('--bkg-names', nargs='+')
    args = args.parse_args()
    logging.info('Args: %s', args)
    return args


def _main():
    args = _get_args()

    bkg_path_template = 'Bkg-{}_{}_{}.NNoutput-{}.yields.background.h5'

    data_input, data_output = mbj_nn.utils.group_files(
        mbj_nn.utils.expand_files(args.files),
        group_sig=args.group_sig
    )

    parameters =  mbj_nn.utils.get_parameters(
        data_input[args.setname] if args.parameters is None else args.parameters,
        args.group_sig,
        dtype=int
    )

    bkg_names = args.bkg_names
    if bkg_names is None:
        bkg_names = mbj_nn.utils.get_bkg_names(data_input[args.setname])

    bkg_path = None
    thrs = {}
    zexps = {}
    if args.njob == 1:
        # sequential
        for isGtt, mg, ml in parameters:
            logging.info('isGtt=%d, mg=%d, ml=%d', isGtt, mg, ml)
            if args.save_bkg_yields:
                bkg_path = bkg_path_template.format(
                    isGtt,
                    mg,
                    ml,
                    0 if args.group_sig == 'Gtt' else 1
                )
            thr, zexp = compute_threshold(
                data_input,
                data_output,
                args.group_sig,
                (isGtt, mg, ml),
                bkg_names,
                args.uncert,
                args.max_stat_uncert,
                args.group_index,
                args.setname,
                args.compat,
                bkg_path,
                args.met,
                args.total_bkg_uncert,
                args.min_control_fract
            )
            logging.info('threshold=%f, zexp=%f', thr, zexp)
            zexps[(mg,ml)] = zexp
            thrs[(mg,ml)] = thr
    else:
        payloads = []
        for isGtt, mg, ml in parameters:
            if args.save_bkg_yields:
                bkg_path = bkg_path_template.format(
                    isGtt,
                    mg,
                    ml,
                    0 if args.group_sig == 'Gtt' else 1
                )
            payloads.append((
                data_input,
                data_output,
                args.group_sig,
                (isGtt, mg, ml),
                bkg_names,
                args.uncert,
                args.max_stat_uncert,
                args.group_index,
                args.setname,
                args.compat,
                bkg_path,
                args.met,
                args.total_bkg_uncert,
                args.min_control_fract
            ))
        pool = multiprocessing.Pool(args.njob)
        thresholds = pool.map(_worker, payloads)
        for (_, mg,ml), (thr, zexp) in zip(parameters, thresholds):
            zexps[(mg, ml)] = zexp
            thrs[(mg, ml)] = thr

    nexcl = 0
    for z in zexps.values():
        if z >= 1.64:
            nexcl += 1
    logging.info('excluded=%d', nexcl)

    with open(args.output, 'w') as ofile:
        for mg, ml in sorted(thrs.keys()):
            ofile.write('{} {} {}\n'.format(mg, ml, thrs[(mg, ml)]))
    logging.info('Wrote %s', args.output)


if __name__ == '__main__':
    logging.basicConfig(
        level='INFO',
        format='[%(asctime)s %(filename)s %(levelname)s] %(message)s'
    )
    _main()
