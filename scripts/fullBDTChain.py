#!/usr/bin/env python3

import argparse
import glob
import json
import logging
import random
import os
import resource
import re
import sys

import h5py as h5
import numpy as np

import xgboost as xgb

import mbj_nn.dataset
import mbj_nn.utils
import mbj_nn.plot_utils

def _get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True, help='file list, accepts *')
    args.add_argument('--sel', help = 'json with selections', required=False)
    args.add_argument('--sel-sig', metavar='sel_sig.json', help = 'json with selections for signal', required=False)
    args.add_argument('--branches', metavar = 'branches.list', required=True)
    args.add_argument('--group-to-target', metavar='group_to_target.json', required=True)
    args.add_argument('--model', metavar='model.json', required=True)
    args.add_argument('--restart', nargs=3, help='<restart-weights> <restart-epoch> <restart-val>', default=None)
    args.add_argument('--num-rounds', type = int, default = None)
    args.add_argument('--seed', default = None, type = int)
    args.add_argument('--groups-sig', nargs='+', default=['GGMH'])
    args.add_argument('--evBkg', default = None, type = int)

    args.add_argument('--storeBDT', action = 'store_true')
    args.add_argument('--output', default = 'trained_model', help='json file to save the model')
    args.add_argument('--plotname', default = 'plot')
    return args.parse_args()


def _main():
    args = _get_args()

    if not args.seed:
        seed = random.SystemRandom().randint(1, 2**31 - 1)
    else:
        seed = args.seed

    logging.info("seed: %s", seed)
    np.random.seed(seed)

    logging.info("Loading the hyperparameters")
    with open(args.model, 'r') as ifile:
        hps = json.load(ifile)
    num_rounds = args.num_rounds #Number of itereations for learning algorithm
        
    sel = None
    if args.sel:
        with open(args.sel, 'r') as fsel:
            sel = json.load(fsel)

    sel_sig = None
    if args.sel_sig:
        with open(args.sel_sig, 'r') as fsel_sig:
            sel_sig = json.load(fsel_sig)

    if sel_sig:
        print('Sel sig!')
        print(sel_sig)

    print("Group to target", mbj_nn.dataset.read_group_to_target(args.group_to_target))

    files_train = [ f for f in mbj_nn.utils.expand_files(args.files) if 'training' in f ]
    files_test = [ f for f in mbj_nn.utils.expand_files(args.files) if 'test' in f ]

    branches = mbj_nn.dataset.read_branches(args.branches)

    logging.info("Loading the dataset")
    data_train = mbj_nn.dataset.load(
        files_train,
        branches,
        mbj_nn.dataset.read_group_to_target(args.group_to_target),
        hps=hps,
        sel=sel,
        sel_sig=sel_sig,
        evBkg = args.evBkg
    )
    
    branches_plot = branches
    branches_plot.append("m_higgsino")

#    mbj_nn.plot_utils.plot_inputVars_parameters(data_train, branches, plotname = args.plotname+'_variables')

    data_train_x, data_train_y, data_train_w = data_train
    data_train_y = data_train_y[:,1] #XGBoost needs only 1-D array with the labels, with 0 being background and 1 being signal
#    data_train_w = np.reshape(data_train_w, (data_train_w.size,1))
#    print(data_train_y, data_train_w)
 

    # Compute sum of weights for signal and background and scale them to be equal
    mask_sig = data_train_y == 1
    mask_bkg = data_train_y == 0

    sum_s = data_train_w[mask_sig].sum()
    sum_b = data_train_w[mask_bkg].sum()

    if sum_s > sum_b:
        data_train_w[mask_bkg] = data_train_w[mask_bkg]*sum_s/sum_b
    else:
        data_train_w[mask_sig] = data_train_w[mask_sig]*sum_b/sum_s
   
    # Load the datasets in a format that XGBoost likes
    dtrain = xgb.DMatrix(data_train_x, label=data_train_y, weight=data_train_w)
    


    logging.info("Starting training")
    bdt = xgb.train(hps, dtrain, num_rounds)
    logging.info("Training done")

    if args.storeBDT:
        bdt.save_model(args.output+'.json')

    logging.info("Evaluating training sample")

    BDToutput_train = bdt.predict(dtrain)
 
#    np.set_printoptions(threshold=sys.maxsize)
#    print(BDToutput_train)


    branches.remove("m_higgsino")

    logging.info("Loading test sample")
    data_test = mbj_nn.dataset.load(
        files_test,
        branches,
        mbj_nn.dataset.read_group_to_target(args.group_to_target),
        hps=hps,
        sel=sel,
        sel_sig=sel_sig
    )

    logging.info("Evaluating test sample")

    data_test_x, data_test_y, data_test_w = data_test

    data_test_y = data_test_y[:,1]

#    data_test_w = np.reshape(data_test_w, (data_test_w.size,1))
    
    dtest = xgb.DMatrix(data_test_x, label=data_test_y, weight=data_test_w)
    
    BDToutput_test = bdt.predict(dtest)
    
    masses_train = data_train_x[:,-1]
    masses_test = data_test_x[:,-1]

    ROC_train = mbj_nn.plot_utils.plot_BDT_output_fromDataset_parameters(BDToutput_train, data_train_y, data_train_w, masses_train, BDToutput_test, data_test_y, data_test_w, masses_test, plotname = args.plotname)

#    print("BDT trained and evaluated. AUC Train: ", ROC_train, " AUC Test: ", ROC_test)


if __name__ == '__main__':
 
    print("Hello world")
    logging.basicConfig(
        level='INFO',
        format = '[%(asctime)s %(filename)s %(levelname)s] %(message)s'
    )
    _main()
