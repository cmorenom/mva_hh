#!/usr/bin/env python3

import argparse
import glob
import json
import logging
import random
import os
import resource
import re

import h5py as h5
import numpy as np

import mbj_nn.dataset
import mbj_nn.model
import mbj_nn.utils
import mbj_nn.plot_utils

def _get_args():
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True, help='file list, accepts *')
    args.add_argument('--sel', help = 'json with selections', required=False)
    args.add_argument('--sel-sig', metavar='sel_sig.json', help = 'json with selections for signal', required=False)
    args.add_argument('--branches', metavar = 'branches.list', required=True)
    args.add_argument('--group-to-target', metavar='group_to_target.json', required=True)
    args.add_argument('--model', metavar='model.json', required=True)
    args.add_argument('--restart', nargs=3, help='<restart-weights> <restart-epoch> <restart-val>', default=None)
    args.add_argument('--max-epoch', type = int, default = None)
    args.add_argument('--seed', default = None, type = int)
    args.add_argument('--groups-sig', nargs='+', default=['GGMH'])
    args.add_argument('--evBkg', default = None, type = int)

    args.add_argument('--storeNN', action = 'store_true')
    args.add_argument('--output', default = 'trained_model.h5')
    args.add_argument('--plotname', default = 'plot')
    return args.parse_args()


def _main():
    args = _get_args()

    if not args.seed:
        seed = random.SystemRandom().randint(1, 2**31 - 1)
    else:
        seed = args.seed

    logging.info("seed: %s", seed)
    np.random.seed(seed)

    logging.info("Loading the hyperparameters")
    with open(args.model, 'r') as ifile:
        hps = json.load(ifile)

    sel = None
    if args.sel:
        with open(args.sel, 'r') as fsel:
            sel = json.load(fsel)

    sel_sig = None
    if args.sel_sig:
        with open(args.sel_sig, 'r') as fsel_sig:
            sel_sig = json.load(fsel_sig)

    if sel_sig:
        print('Sel sig!')
        print(sel_sig)

    files_train = [ f for f in mbj_nn.utils.expand_files(args.files) if 'training' in f ]
    files_test = [ f for f in mbj_nn.utils.expand_files(args.files) if 'test' in f ]

    branches = mbj_nn.dataset.read_branches(args.branches)

    logging.info("Loading the dataset")
    data_train = mbj_nn.dataset.load(
        files_train,
        branches,
        mbj_nn.dataset.read_group_to_target(args.group_to_target),
        hps=hps,
        sel=sel,
        sel_sig=sel_sig,
        evBkg = args.evBkg
    )
    

#    mbj_nn.plot_utils.plot_inputVars(data_train, branches, plotname = args.plotname+'_variables')

    logging.info("Setting up model for training")
    nn, fitcfg, norm = mbj_nn.model.setup_training(
        hps=hps,
        data=data_train,
        output_path=args.output,
        restart=args.restart
    )

    if args.max_epoch:
        logging.warning("Running on a maximum of %d epochs", args.max_epoch)
        fitcfg['epochs'] = args.max_epoch

    logging.info("Starting training")
    hist = nn.fit(**fitcfg)
    logging.info("Training done")

#    files_for_params = [ p for p in args.files if os.path.basename(p).startswith('Sig_') ]

#    parameters = mbj_nn.utils.get_parameters(files_for_params, groups_sig=args.groups_sig)
    
#    if parameters.shape[0] == 0:
#        logging.warning('No parameters!')


    logging.info("Evaluating training sample")

    data_train_x, data_train_y, data_train_w = data_train

    mean = norm['mean']
    std = norm['std']
    print(norm)
    print(mean, std)
    data_train_x -= mean
    data_train_x /= std

    NNoutput_train = nn.predict(data_train_x, batch_size=1024)

    print(NNoutput_train)

    logging.info("Loading test sample")
    data_test = mbj_nn.dataset.load(
        files_test,
        branches,
        mbj_nn.dataset.read_group_to_target(args.group_to_target),
        hps=hps,
        sel=sel,
        sel_sig=sel_sig
    )

    logging.info("Evaluating test sample")

    data_test_x, data_test_y, data_test_w = data_test

    data_test_x -= mean
    data_test_x /= std
    NNoutput_test = nn.predict(data_test_x, batch_size=1024)

    print(NNoutput_test)

    mbj_nn.plot_utils.plot_NN_output_fromDataset(NNoutput_train, data_train_y, data_train_w, NNoutput_test, data_test_y, data_test_w, plotname = args.plotname)

    
if __name__ == '__main__':
 
    print("Hello world")
    logging.basicConfig(
        level='INFO',
        format = '[%(asctime)s %(filename)s %(levelname)s] %(message)s'
    )
    _main()
