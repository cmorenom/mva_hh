import argparse
import os

def _get_args():
    args=argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True)
    args.add_argument('--output')

    return args.parse_args()

def _main():

    args=_get_args()

    for path in args.files:
        suf = '.BDToutput.h5'
        opath = args.output+os.path.basename(path) + suf

        print(opath)

if __name__ == '__main__':

    _main()
