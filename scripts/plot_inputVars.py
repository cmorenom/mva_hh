#!/usr/bin/env python3

import argparse
import glob
import json
import logging
import random
import sys

import h5py as h5
import numpy as np

import mbj_nn.dataset
import mbj_nn.utils
import mbj_nn.plot_utils

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--files', nargs='+', required=True, help='file list, accepts *')
    parser.add_argument('--sel', help='json with selections', required=False)
    parser.add_argument('--sel-sig', metavar='sel_sig.json', help='json with selections for  signals (e.g. only hh->4b)', required=False)
    parser.add_argument('--branches', metavar='branches.list', required=True)
    parser.add_argument('--group-to-target', metavar='group_to_target.json', required=True)
    parser.add_argument('--plotname', default = 'plot')

    args = parser.parse_args()

    sel=None
    if args.sel:
        with open(args.sel, 'r') as fsel:
            sel = json.load(fsel)

    print(sel)

    for key in sel:
        print(key, sel[key])

    sel_sig=None
    if args.sel_sig:
        with open(args.sel_sig, 'r') as fsel_sig:
            sel_sig = json.load(fsel_sig)


    branches = mbj_nn.dataset.read_branches(args.branches)

       
    logging.info("Loading the dataset")
    data = mbj_nn.dataset.load(
        mbj_nn.utils.expand_files(args.files), # expands * in args.files
        mbj_nn.dataset.read_branches(args.branches), # branches.strip() as list
        mbj_nn.dataset.read_group_to_target(args.group_to_target), # loads dictionary
        hps={},
        sel=sel,
        sel_sig=sel_sig
    )
    
    separation = mbj_nn.plot_utils.plot_inputVars_parameters(data, branches, plotname = args.plotname+'_variables')
    """
    f = open("variable_ranking.txt", "w")
    firstline = True
    for vari in separation:
        sep = separation[vari]
        if firstline:
            f.write("Variables\t")
            for mass in sorted(sep):
                f.write(str(int(mass))+"\t")
            f.write("\n")
            firstline=False 
        f.write(str(vari)+"\t")
        for mass in sorted(sep):
            f.write(str(sep[mass])+"\t")
        f.write("\n")
    f.close()
    """
#    mbj_nn.plot_utils.plot_correlation_matrix(data, branches, plotname = args.plotname+'_correlation')
