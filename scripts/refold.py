#!/bin/env python
import argparse
import glob
import os
import shutil
import subprocess
import tempfile

import numpy as np
import ROOT
import root_numpy

args = argparse.ArgumentParser()
args.add_argument('train')
args.add_argument('test')
args = args.parse_args()

if not args.train.endswith('training'):
    raise RuntimeError("Invalid .training file")

if not args.test.endswith('test'):
    raise RuntimeError("Invalid .test file")

tf_train = ROOT.TFile(args.train, 'READ')
tf_test = ROOT.TFile(args.test, 'READ')

train_keys = {k.GetName().split(';')[0] for k in tf_train.GetListOfKeys()}
test_keys = {k.GetName().split(';')[0] for k in tf_test.GetListOfKeys()}

if train_keys != test_keys:
    raise RuntimeError('tree mismatch between train and test')

outprefix = os.path.basename(args.train).replace('.training', '')

tmpd = tempfile.mkdtemp(dir=os.getcwd())
tmp = '{}/tmp.root'.format(tmpd)

print('==> Merging input files...')
subprocess.check_call([
    'hadd',
    tmp,
    args.train,
    args.test
])

print('==> Splitting all trees...')
tf_all = ROOT.TFile(tmp, 'READ')
for treename in train_keys:
    ttree = tf_all.Get(treename)
    mid = int(0.5 * ttree.GetEntries())
    print('  -> {}:1'.format(treename))
    subprocess.check_call([
        'rooteventselector',
        '-l', str(mid),
        '{}:{}'.format(tmp, treename),
        '{}/{}.1.root'.format(tmpd, treename)
    ])
    print('  -> {}:2'.format(treename))
    subprocess.check_call([
        'rooteventselector',
        '-f', str(mid + 1),
        '{}:{}'.format(tmp, treename),
        '{}/{}.2.root'.format(tmpd, treename)
    ])

print('==> Merging *.1.root...')
print('  -> Merging')
subprocess.check_call([
    'hadd',
    '{}/{}.1.root'.format(tmpd, outprefix),
] + glob.glob('{}/*.1.root'.format(tmpd)))
print('  -> Reweighting')
subprocess.check_call([
    'reweight',
    tmp,
    '{}/{}.1.root'.format(tmpd, outprefix),
    '{}.1.root'.format(outprefix)
])

print('==> Merging *.2.root...')
print('  -> Merging')
subprocess.check_call([
    'hadd',
    '{}/{}.2.root'.format(tmpd, outprefix),
] + glob.glob('{}/*.2.root'.format(tmpd)))
print('  -> Reweighting')
subprocess.check_call([
    'reweight',
    tmp,
    '{}/{}.2.root'.format(tmpd, outprefix),
    '{}.2.root'.format(outprefix)
])

print('==> Validating...')
for treename in train_keys:
    ref = root_numpy.root2array(
        tmp,
        treename=treename,
        branches=['small_R_jets_pt_0', 'weight', 'weight_lumi_real']
    )
    dat1 = root_numpy.root2array(
        '{}.1.root'.format(outprefix),
        treename=treename,
        branches=['small_R_jets_pt_0', 'weight', 'weight_lumi_real']
    )
    dat2 = root_numpy.root2array(
        '{}.2.root'.format(outprefix),
        treename=treename,
        branches=['small_R_jets_pt_0', 'weight', 'weight_lumi_real']
    )
    np.testing.assert_allclose(
        ref['small_R_jets_pt_0'],
        np.concatenate(
            (dat1['small_R_jets_pt_0'], dat2['small_R_jets_pt_0'])
        )
    )
    np.testing.assert_allclose(
        ref['weight'],
        np.concatenate(
            (dat1['weight'], dat2['weight'])
        )
    )
    np.testing.assert_allclose(
        np.sum(ref['weight_lumi_real']),
        np.sum(dat1['weight_lumi_real'])
    )
    np.testing.assert_allclose(
        np.sum(dat1['weight_lumi_real']),
        np.sum(dat2['weight_lumi_real'])
    )

print('==> Cleaning up...')
shutil.rmtree(tmpd)
print('==> All Done :-)')
