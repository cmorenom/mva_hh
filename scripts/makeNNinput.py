#!/usr/bin/env python

import argparse
import os
import subprocess

args = argparse.ArgumentParser()
args.add_argument('--path', required=True, nargs='+')
args.add_argument('--nsmall', type=int, default=10)
args.add_argument('--nlarge', type=int, default=4)
args.add_argument('--nlepton', type=int, default=4)
args.add_argument('--all-pass', action='store_true')
args.add_argument('--pxpypze', action='store_true')
args.add_argument('--is-data', action='store_true')
args.add_argument('--output')
args = args.parse_args()

paths = []
for p in args.path:
    if ',' in p:
        paths += p.split(',')
    else:
        paths.append(p)

for i, p in enumerate(paths):
    print('INFO path#{}: {}'.format(i + 1, p))

outs = []

for path in paths:
    outs.append(os.path.basename(path) + '.NNinput')
    subprocess.check_call(
        "makeNNinput {} {} {} {} {} {} {} {}".format(
            path,
            outs[-1],
            args.nsmall,
            args.nlarge,
            args.nlepton,
            '1' if args.all_pass else '0',
            '1' if args.pxpypze else '0',
            '1' if args.is_data else '0'
        ),
        shell=True
    )

if args.output:
    if len(outs) > 1:
        subprocess.check_call(
            "hadd {} {}".format(args.output, " ".join(outs)),
            shell=True
        )
    elif outs[0] != args.output:
        subprocess.check_call(
            "mv --verbose {} {}".format(outs[0], args.output),
            shell=True
        )
    else:
        exit(0)
    subprocess.check_call(
        "rm --verbose {}".format(" ".join(outs)),
        shell=True
    )
