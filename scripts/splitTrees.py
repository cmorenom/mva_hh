#!/usr/bin/env python

import mbj_nn.utils
import argparse
import subprocess
import tempfile
import os

import numpy as np

args = argparse.ArgumentParser()
args.add_argument('--path', nargs='+', required=True, help='path to files, accepts *')
args.add_argument('--fractions', type=float, nargs=3, required=True, help='train, validation, test fractions')
args.add_argument('--seed', type=int, help='seed')
args = args.parse_args()

if not np.isclose(np.sum(args.fractions), 1):
    raise RuntimeError("fractions don't sum to 1")

train_frac, valid_frac, test_frac = [str(f) for f in args.fractions]

files = mbj_nn.utils.expand_files(args.path)

for f in files:     
    to_call = "splitTrees {} {} {} {} {}".format(f, train_frac, valid_frac, test_frac, "" if not args.seed else str(args.seed))
    print('\n',to_call)
    subprocess.check_call(
        to_call,
        shell=True
        )

    for fold in ['training', 'validation', 'test']:
        tmp = tempfile.mktemp()
        torw = os.path.basename(f) + '.' + fold
        to_call = "reweight {} {} {}".format(f, torw, tmp)
        print('\n',to_call)
        subprocess.check_call(
            to_call,
            shell=True
            )
        to_call = "mv -v {} {}".format(tmp, torw)
        print('\n',to_call)
        subprocess.check_call(to_call, shell=True)

