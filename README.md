
Framework to optimize a NN analysis. 
Forked from @lgagnon code [here](https://gitlab.cern.ch/lgagnon/mbj_nn)

- [Workflow](#workflow)
    + [0) Preparation](#0--preparation)
    + [1) Create train, validation and test set](#1--create-train--validation-and-test-set)
    + [2) Training](#2--training)
    + [3) Apply the weights to the trees](#3--apply-the-weights-to-the-trees)
    + [Extra: places where naming conventions matter](#extra--places-where-naming-conventions-matter)
- [Docker](#docker)
- [Hyperparameter Optimization](#hyperparameter-optimization)
- [SR definition](#sr-definition)
- [TO DO](#to-do)

## Workflow 

#### 0) Preparation 

Compilation and setups:
* Run `make` (note: requires ROOT, to run inside docker see [later](#docker))
* Run `source setup.sh`

Files to prepare in advance:
* Expected input: root flat TTrees for signal and background processes  
* Text file with the list of branches to be considered, e.g. [this one](./config/branches_lowlevel.list)
* JSON file with the definition of your model, e.g. [this one](./config/model.json)
* JSON file with association between tree name and output cathegory, e.g. [this one](./config/group_to_target.json)
* Do you want to apply some preselection to your samples? Then you need a JSON file like [this one](./config/sel.json) 
containing the selections. This is not required if you want to use all the events in your ROOT files 
* Do you want to apply some signal-specific selections (e.g. consider only hh->4b in the training)? 
Then you need a JSON file like [this one](config/sel_sig.json). This is not required if there are no signal-specific selections 
you want to apply. 

#### 1) Create train, validation and test set

Split the each input TTree into three: one for training, one for validation and one for testing. 

This is done with the script [splitTrees.py](./scripts/splitTrees.py):

```
python scripts/splitTrees.py --path path-to-your-files/* --fractions 0.3 0.35 0.35 --seed 277
```

The script runs two c++ programs: 
* [splitTrees](./src/splitTrees.cxx) to split the trees
* [reweight](./src/reweight.cxx) reweight to change the branches with the luminosity weight (note: the script needs to be adapted to change also weight_lumi_up and weight_lumi_down) 

#### 2) Training 

Run the training (on the training set) with: 
```
python scripts/trainNN.py --files *root.training --sel config/sel.json --sel-sig config/sel_sig.json --branches config/branches_lowlevel.list --group-to-target config/group_to_target.json --model config/model.json --output mymodel.h5 --seed 277
```                          

#### 3) Apply the weights to the trees 

Evaluate to create h5 file
```
python scripts/evalNN.py --files ../ntuples/21.2.87-EWK/3b/*root --branches config/branches_lowlevel.list --model trained_model_100epochs_low_level.h5 --no-signal-match
```
Turn the h5 file into a root file and add the root file as a friend to the original root file 
```
python scripts/NNoutput_h5_to_root.py --files ../ntuples/21.2.87-EWK/3b/validation/* ../ntuples/21.2.87-EWK/3b/data/* 
```

#### Extra: places where naming conventions matter

There are a few points where things might fail if your files/samples have a naming structure different than what expected. E.g.:
* Regular expression to identify signal samples and extract parameters
  + In particular, check [dataset.py](./python/mbj_nn/dataset.py) and [utils.py](./python/mbj_nn/utils.py) for expressions containing the expected signal name structure, e.g. `'shh_zp([0-9]*)_dm200_dh([0-9]*)'`
* Name pattern to distinguish between train, validation, test sets and data when evaluating the NN (e.g. in the `group_files` function in [utils.py](./python/mbj_nn/utils.py))

## Docker

To run this inside the [atlasml-base](https://gitlab.cern.ch/aml/containers/docker/-/tree/master/) docker image: 
```
docker run --rm -it -v $PWD:$PWD  -p 8888:8888 atlasml/atlasml-base:py-3.6.8 
source /home/atlas/release_setup.sh
cd path-to-hh_nn
source /opt/lcg/binutils/2.28/x86_64-slc6/setup.sh
source setup.sh 
make 
python3 scripts/splitTrees.py ... 
python3 scripts/trainNN.py ... 
```
Notes: 
* If you want to run on local root files, you need to make sure that they are inside the path you mount on docker (path after the option `-v`)

## SR definition 

Useful scripts:
* [setThreshold.py](./scripts/setThreshold.py)
* [computeSignificance.py](./scripts/computeSignificance.py)

## Hyperparameter Optimization

* Create several JSON model files with a script like [this](./models/generators/generator_1.py) and run training for each of them 
  + Need to be able to run on the grid before doing this
* Find SRs for each and use significance to pick the best 

## TO DO

* How to run hyperparameter optimization on the grid? 
  + In particular, how to read rucio datasets instead of local ROOT files 

## For bookkeeping: main changes wrt L-G's code 

* Possibility to add extra  selections for ROOT files and signal-specific selections 
* Small changes to scripts, e.g. possibility to have `*` in [splitTrees.py](./scripts/splitTrees.py)