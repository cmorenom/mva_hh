nohup python3 scripts/fullNNChain.py --files ../samples/training/* ../samples/test/* --sel config/sel.json --sel-sig config/sel_sig.json --branches config/branches_highlevel.list --group-to-target config/group_to_target_500.json --model config/model.json --evBkg 15000 --plotname 'NN_500_highlvl' --output 'model_NN_500_highlvl.h5' &

nohup python3 scripts/fullNNChain.py --files ../samples/training/* ../samples/test/* --sel config/sel.json --sel-sig config/sel_sig.json --branches config/branches_highlevel.list --group-to-target config/group_to_target_1100.json --model config/model.json --evBkg 15000 --plotname 'NN_1100_highlvl' --output 'model_NN_1100_highlvl.h5' &


nohup python3 scripts/fullNNChain.py --files ../samples/training/* ../samples/test/* --sel config/sel.json --sel-sig config/sel_sig.json --branches config/branches_lowlevel.list --group-to-target config/group_to_target_500.json --model config/model.json --evBkg 15000 --plotname 'NN_500_lowlvl' --output 'model_NN_500_lowlvl.h5' &

nohup python3 scripts/fullNNChain.py --files ../samples/training/* ../samples/test/* --sel config/sel.json --sel-sig config/sel_sig.json --branches config/branches_lowlevel.list --group-to-target config/group_to_target_1100.json --model config/model.json --evBkg 15000 --plotname 'NN_1100_lowlvl' --output 'model_NN_1100_lowlvl.h5' &


nohup python3 scripts/fullNNChain.py --files ../samples/training/* ../samples/test/* --sel config/sel.json --sel-sig config/sel_sig.json --branches config/branches_intlevel.list --group-to-target config/group_to_target_500.json --model config/model.json --evBkg 15000 --plotname 'NN_500_intlvl' --output 'model_NN_500_intlvl.h5' &

nohup python3 scripts/fullNNChain.py --files ../samples/training/* ../samples/test/* --sel config/sel.json --sel-sig config/sel_sig.json --branches config/branches_intlevel.list --group-to-target config/group_to_target_1100.json --model config/model.json --evBkg 15000 --plotname 'NN_1100_intlvl' --output 'model_NN_1100_intlvl.h5' &
