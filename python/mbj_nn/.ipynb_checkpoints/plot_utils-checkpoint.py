import glob
import logging
import os
import re

import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
#import ROOT
from array import array

from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score

def selection_to_string(sel):
    
    sel_string = ""
    
    for branch in sel:
        cuts = sel[branch]
        if isinstance(cuts, list):
            sel_string+=branch+cuts[0]+str(cuts[1])+"&&"
        elif isinstance(cuts, dict):
            for key in cuts:
                sel_string+=branch+cuts[key][0]+str(cuts[key][1])+"&&"

    return sel_string[:-2]

def guess_bin_and_range(data, branch):

    ibin = int(abs(np.max(data) - np.min(data)))
    if ibin > 75:
        ibin = 75

    if 'dR_' in branch or 'dphi' in branch:
        ibin = ibin*10

    irange = [np.min(data),np.max(data)]
    
    if 'm_' in branch:
        irange = [np.min(data), np.percentile(data, 99)]

    return ibin, irange

def plot_inputVars(data, branches, plotname = 'variableplot'):
    """ Plot the input variables used for the NN training with signal and background together """

    if len(data) == 3:
        data_x, data_y, data_w = data
        data_w_tr = None
    elif len(data) == 4:
        data_x, data_y, data_w, data_w_tr = data

    mask_bkg = data_y[:,0] == 1
    mask_sig = data_y[:,1] == 1

    with PdfPages(plotname+'.pdf') as pdf:
        for vari in range(0, len(branches)):
 
            print(vari, branches[vari])
            
            bkg = data_x[mask_bkg,vari]
            sig = data_x[mask_sig,vari]
            wei = np.reshape(data_w, (data_w.size,1))
            
            sum_b = wei[mask_bkg].sum()
            sum_s = wei[mask_sig].sum()
            
            print(sum_b, sum_s)
            
            wei_b = wei[mask_bkg]/sum_b
            wei_s = wei[mask_sig]/sum_s
    
            print("Background: ", bkg)
            
            ibin, irange = guess_bin_and_range(data_x[:,vari], branches[vari])
            
            print(ibin, irange)

            x_b, bins_b, p_b = plt.hist(bkg, ibin, range=irange, weights=wei_b, label='Background', color='r', density=False, alpha=0.5, histtype='stepfilled')
            x_s, bins_s, p_s = plt.hist(sig, ibin, range=irange, weights=wei_s, label='Signal', color='b', density=False, alpha=0.5, histtype='stepfilled')
#            x_s, bins_s, p_s = plt.hist(sig, bin, range=[np.min(data_x[:,vari]),np.percentile(data_x[:,vari], 99)], weights=wei_s, label='Signal', color='b', density=False, alpha=0.5, histtype='stepfilled')
        
            plt.xlabel(branches[vari])
            plt.ylabel('Rate of events')
            plt.legend(loc='upper right')
            plt.show()
            pdf.savefig()
            plt.close()

    return

def plot_NN_output_fromDataset(data_train_x, data_train_y, data_train_w, data_test_x, data_test_y, data_test_w, plotname = 'NNoutput'):
    """ Plot the NN output from the numpy arrays. Plots only probability of signal """

    # Create masks for signal and background
    mask_bkg_train = data_train_y[:,0] == 1
    mask_sig_train = data_train_y[:,1] == 1

    mask_bkg_test = data_test_y[:,0] == 1
    mask_sig_test = data_test_y[:,1] == 1

    # Normalize the distributions to compare signal vs background
    # Get column 1 from NN output because that's P(sig)
    bkg_train = data_train_x[mask_bkg_train,1]
    sig_train = data_train_x[mask_sig_train,1]
    wei_train = np.reshape(data_train_w, (data_train_w.size,1))

    sum_train_b = wei_train[mask_bkg_train].sum()
    sum_train_s = wei_train[mask_sig_train].sum()

    wei_train_b = wei_train[mask_bkg_train]/sum_train_b
    wei_train_s = wei_train[mask_sig_train]/sum_train_s

    bkg_test = data_test_x[mask_bkg_test,1]
    sig_test = data_test_x[mask_sig_test,1]
    wei_test = np.reshape(data_test_w, (data_test_w.size,1))
    
    sum_test_b = wei_test[mask_bkg_test].sum()
    sum_test_s = wei_test[mask_sig_test].sum()

    wei_test_b = wei_test[mask_bkg_test]/sum_test_b
    wei_test_s = wei_test[mask_sig_test]/sum_test_s

    x_train_b, bins_train_b, p_train_b = plt.hist(bkg_train, 50, range=[0,1], weights=wei_train_b, label='Background - Training Sample', color='r', density=False, alpha=0.5, histtype='stepfilled')
    
    x_train_s, bins_train_s, p_train_s = plt.hist(sig_train, 50, range=[0,1], weights=wei_train_s, label='Signal - Training Sample', color='b', density=False, alpha=0.5, histtype='stepfilled')

    x_test_b, bins_test_b, p_test_b = plt.hist(bkg_test, 50, range=[0,1], weights=wei_test_b, label='Background - Test Sample', color='r', histtype='step')

    x_test_s, bins_test_s, p_test_s = plt.hist(sig_test, 50, range=[0,1], weights=wei_test_s, label='Signal - Test Sample', color='b', histtype='step')

    plt.xlabel('NN output')
    plt.ylabel('Rate of events')
    plt.legend(loc='upper center')
    plt.show()
    plt.savefig(plotname+'.png')
    plt.close()

    # Create new histograms with finer binning to compute the ROC curve
    x_train_b, bins_train_b, p_train_b = plt.hist(bkg_train, 200, range=[0,1], weights=wei_train_b)
    x_train_s, bins_train_s, p_train_s = plt.hist(sig_train, 200, range=[0,1], weights=wei_train_s)

    train_b_integral = 1.*x_train_b.sum()
    train_s_integral = 1.*x_train_s.sum()

    x_train_b = x_train_b[::-1]
    x_train_s = x_train_s[::-1]

    sig_eff_train = []
    bkg_rej_train = []
    s, b = 0, 0
    AUC_train = 0
    for ibin in range(len(x_train_b)):
        s += x_train_s[ibin]
        b += x_train_b[ibin]
        sig_eff_train.append(s/train_s_integral)
        bkg_rej_train.append(1.-b/train_b_integral)

        AUC_train += (1. -b/train_b_integral)/(1.*len(x_train_b)) # integral += y(x)*(1/nbins_x)

    x_test_b, bins_test_b, p_test_b = plt.hist(bkg_test, 200, range=[0,1], weights=wei_test_b)
    x_test_s, bins_test_s, p_test_s = plt.hist(sig_test, 200, range=[0,1], weights=wei_test_s)

    test_b_integral = 1.*x_test_b.sum()
    test_s_integral = 1.*x_test_s.sum()

    x_test_b = x_test_b[::-1]
    x_test_s = x_test_s[::-1]

    sig_eff_test = []
    bkg_rej_test = []
    s, b = 0, 0 
    AUC_test = 0
    for ibin in range(len(x_test_b)):
        s += x_test_s[ibin]
        b += x_test_b[ibin]
        sig_eff_test.append(s/test_s_integral)
        bkg_rej_test.append(1.-b/test_b_integral)
        
        AUC_test += (1. - b/test_b_integral)/(1.*len(x_test_b))

    plt.close()

    label_train = 'Training sample - AUC: '+ str(AUC_train)
    label_test = 'Test sample - AUC: '+ str(AUC_test)

    plt.plot(sig_eff_train, bkg_rej_train, color='red', linewidth=2, label=label_train)
    plt.plot(sig_eff_test, bkg_rej_test, color='red', linewidth=2, linestyle='dashed', label=label_test)
    
    plt.xlabel('Signal efficiency')
    plt.ylabel('Background rejection')
    plt.legend(loc='lower left')
    plt.show()
    plt.savefig(plotname+'_ROC.png')

    plt.close()

    return
    
def plot_BDT_output_fromDataset(data_train_x, data_train_y, data_train_w, data_test_x, data_test_y, data_test_w, plotname = 'NNoutput'):
    """ Plot the NN output from the numpy arrays. Plots only probability of signal """

    # Create masks for signal and background
    mask_bkg_train = data_train_y == 0
    mask_sig_train = data_train_y == 1

    mask_bkg_test = data_test_y == 0
    mask_sig_test = data_test_y == 1

    # Normalize the distributions to compare signal vs background
    # Get column 1 from NN output because that's P(sig)
    bkg_train = data_train_x[mask_bkg_train]
    bkg_train = np.reshape(bkg_train, (bkg_train.size,1))
    sig_train = data_train_x[mask_sig_train]
    sig_train = np.reshape(sig_train, (sig_train.size,1))
    wei_train = np.reshape(data_train_w, (data_train_w.size,1))
    
    sum_train_b = wei_train[mask_bkg_train].sum()
    sum_train_s = wei_train[mask_sig_train].sum()

    wei_train_b = wei_train[mask_bkg_train]/sum_train_b
    wei_train_s = wei_train[mask_sig_train]/sum_train_s

    bkg_test = data_test_x[mask_bkg_test]
    bkg_test = np.reshape(bkg_test, (bkg_test.size,1))
    sig_test = data_test_x[mask_sig_test]
    sig_test = np.reshape(sig_test, (sig_test.size,1))
    wei_test = np.reshape(data_test_w, (data_test_w.size,1))
    
    sum_test_b = wei_test[mask_bkg_test].sum()
    sum_test_s = wei_test[mask_sig_test].sum()

    wei_test_b = wei_test[mask_bkg_test]/sum_test_b
    wei_test_s = wei_test[mask_sig_test]/sum_test_s
    
    x_train_b, bins_train_b, p_train_b = plt.hist(bkg_train, 50, range=[0,1], weights=wei_train_b, label='Background - Training Sample', color='r', density=False, alpha=0.5, histtype='stepfilled')
    
    x_train_s, bins_train_s, p_train_s = plt.hist(sig_train, 50, range=[0,1], weights=wei_train_s, label='Signal - Training Sample', color='b', density=False, alpha=0.5, histtype='stepfilled')

    x_test_b, bins_test_b, p_test_b = plt.hist(bkg_test, 50, range=[0,1], weights=wei_test_b, label='Background - Test Sample', color='r', histtype='step')

    x_test_s, bins_test_s, p_test_s = plt.hist(sig_test, 50, range=[0,1], weights=wei_test_s, label='Signal - Test Sample', color='b', histtype='step')

    plt.xlabel('BDT output')
    plt.ylabel('Rate of events')
    plt.legend(loc='upper center')
    plt.show()
    plt.savefig(plotname+'.png')
    plt.close()

    # Create new histograms with finer binning to compute the ROC curve
    x_train_b, bins_train_b, p_train_b = plt.hist(bkg_train, 200, range=[0,1], weights=wei_train_b)
    x_train_s, bins_train_s, p_train_s = plt.hist(sig_train, 200, range=[0,1], weights=wei_train_s)

    train_b_integral = 1.*x_train_b.sum()
    train_s_integral = 1.*x_train_s.sum()

    x_train_b = x_train_b[::-1]
    x_train_s = x_train_s[::-1]

    sig_eff_train = []
    bkg_rej_train = []
    s, b = 0, 0
    AUC_train = 0
    for ibin in range(len(x_train_b)):
        s += x_train_s[ibin]
        b += x_train_b[ibin]
        sig_eff_train.append(s/train_s_integral)
        bkg_rej_train.append(1.-b/train_b_integral)

        AUC_train += (1. -b/train_b_integral)/(1.*len(x_train_b)) # integral += y(x)*(1/nbins_x)

    x_test_b, bins_test_b, p_test_b = plt.hist(bkg_test, 200, range=[0,1], weights=wei_test_b)
    x_test_s, bins_test_s, p_test_s = plt.hist(sig_test, 200, range=[0,1], weights=wei_test_s)

    test_b_integral = 1.*x_test_b.sum()
    test_s_integral = 1.*x_test_s.sum()

    x_test_b = x_test_b[::-1]
    x_test_s = x_test_s[::-1]

    sig_eff_test = []
    bkg_rej_test = []
    s, b = 0, 0 
    AUC_test = 0
    for ibin in range(len(x_test_b)):
        s += x_test_s[ibin]
        b += x_test_b[ibin]
        sig_eff_test.append(s/test_s_integral)
        bkg_rej_test.append(1.-b/test_b_integral)
        
        AUC_test += (1. - b/test_b_integral)/(1.*len(x_test_b))

    plt.close()

    label_train = 'Training sample - AUC: '+ str(AUC_train)
    label_test = 'Test sample - AUC: '+ str(AUC_test)

    plt.plot(sig_eff_train, bkg_rej_train, color='red', linewidth=2, label=label_train)
    plt.plot(sig_eff_test, bkg_rej_test, color='red', linewidth=2, linestyle='dashed', label=label_test)
    
    plt.xlabel('Signal efficiency')
    plt.ylabel('Background rejection')
    plt.legend(loc='lower left')
    plt.show()
    plt.savefig(plotname+'_ROC.png')

    plt.close()

    return
    

def plot_NN_output(signal_files, signal_tree, background_files, background_tree, selection, branch, selection_signal = None, signal_test = None, background_test = None, plot_ROC = False):
    
    ROOT.gStyle.SetOptStat(0)

    t_sig = ROOT.TChain(signal_tree)
    for f_sig in signal_files:
        t_sig.AddFile(f_sig)

    t_bkg = ROOT.TChain(background_tree)
    for f_bkg in background_files:
        t_bkg.AddFile(f_bkg)

    t_sig_test = None
    t_bkg_test = None

    if signal_test:
        t_sig_test = ROOT.TChain(signal_tree)
        for f_sig_test in signal_test:
            t_sig_test.AddFile(f_sig_test)
        
    if background_test:
        t_bkg_test = ROOT.TChain(background_tree)
        for f_bkg_test in background_test:
            t_bkg_test.AddFile(f_bkg_test)

    h_sig = ROOT.TH1F("h_sig", "h_sig", 50, -0.01, 1.01)
    h_bkg = ROOT.TH1F("h_bkg", "h_bkg", 50, -0.01, 1.01)
    h_bkg_test = ROOT.TH1F("h_bkg_test", "h_bkg_test", 50, -0.01, 1.01)
    h_sig_test = ROOT.TH1F("h_sig_test", "h_sig_test", 50, -0.01, 1.01)

    c1 = ROOT.TCanvas()
    c1.SetGrid()
    c1.SetLogy()

    t_bkg.Draw(branch+">>h_bkg", selection, "hist")
    h_bkg.SetFillColor(ROOT.kRed-9);
    h_bkg.SetFillStyle(3354);
    h_bkg.SetLineColor(ROOT.kRed-9);
    h_bkg.SetLineWidth(2);

    if selection_signal:
        t_sig.Draw(branch+">>h_sig", selection_signal, "hist same")
    else:
        t_sig.Draw(branch+">>h_sig", selection, "hist same")
    h_sig.SetFillColor(ROOT.kBlue-9);
    h_sig.SetFillStyle(3345);
    h_sig.SetLineColor(ROOT.kBlue-9);
    h_sig.SetLineWidth(2);

    if t_bkg_test:
        t_bkg_test.Draw(branch+">>h_bkg_test", selection, "p same")
        h_bkg_test.SetMarkerColor(ROOT.kRed+1);
        h_bkg_test.SetLineColor(ROOT.kRed+1);
        h_bkg_test.SetMarkerStyle(8);

    if t_sig_test:
        if selection_signal:
            t_sig_test.Draw(branch+">>h_sig_test", selection_signal, "p same")
        else:
            t_sig_test.Draw(branch+">>h_sig_test", selection, "p same")
        h_sig_test.SetMarkerColor(ROOT.kBlue+1);
        h_sig_test.SetLineColor(ROOT.kBlue+1);
        h_sig_test.SetMarkerStyle(8);

    # Normalize histograms
    h_sig.Scale(1./h_sig.Integral())
    h_bkg.Scale(1./h_bkg.Integral())
    
    if t_bkg_test:
        h_bkg_test.Scale(1./h_bkg_test.Integral())
    if t_sig_test:
        h_sig_test.Scale(1./h_sig_test.Integral())

    y_bkg = h_bkg.GetBinContent(h_bkg.GetMaximumBin())
    y_sig = h_sig.GetBinContent(h_sig.GetMaximumBin())

    y_bkg_test = 0.
    y_sig_test = 0.

    if t_bkg_test:
        y_bkg_test = h_bkg_test.GetBinContent(h_bkg_test.GetMaximumBin())
    if t_sig_test:
        y_sig_test = h_sig_test.GetBinContent(h_sig_test.GetMaximumBin())

    h_bkg.GetYaxis().SetRangeUser(1e-4, 1.2*max(y_bkg, y_sig, y_bkg_test, y_sig_test))
    h_bkg.GetXaxis().SetTitle("NN Output")
    h_bkg.GetYaxis().SetTitle("Rate of events")
    h_bkg.SetTitle("NN Output for signal and background")
    
    legend = ROOT.TLegend(0.3,0.6,0.7,0.9)
    legend.AddEntry(h_bkg, "Background training sample")
    legend.AddEntry(h_sig, "Signal training sample")
    if t_bkg_test:
        legend.AddEntry(h_bkg_test, "Background test sample")
    if t_sig_test:
        legend.AddEntry(h_sig_test, "Signal test sample")
    legend.Draw()

    c1.Update()
    c1.Modified()
    c1.SaveAs("NNoutput_plot.png")

    return



def plot_ROC(signal_files, signal_tree, background_files, background_tree, selection, branch, selection_signal = None, signal_test = None, background_test = None, nbins = 50):

    ROOT.gStyle.SetOptStat(0)

    t_sig = ROOT.TChain(signal_tree)
    for f_sig in signal_files:
        t_sig.AddFile(f_sig)

    t_bkg = ROOT.TChain(background_tree)
    for f_bkg in background_files:
        t_bkg.AddFile(f_bkg)

    t_sig_test = None
    t_bkg_test = None

    if signal_test:
        t_sig_test = ROOT.TChain(signal_tree)
        for f_sig_test in signal_test:
            t_sig_test.AddFile(f_sig_test)
        
    if background_test:
        t_bkg_test = ROOT.TChain(background_tree)
        for f_bkg_test in background_test:
            t_bkg_test.AddFile(f_bkg_test)

    h_sig = ROOT.TH1F("h_sig", "h_sig", nbins, -0.01, 1.01)
    h_bkg = ROOT.TH1F("h_bkg", "h_bkg", nbins, -0.01, 1.01)
    h_bkg_test = ROOT.TH1F("h_bkg_test", "h_bkg_test", nbins, -0.01, 1.01)
    h_sig_test = ROOT.TH1F("h_sig_test", "h_sig_test", nbins, -0.01, 1.01)


    # Fill all the needed histograms

    t_bkg.Draw(branch+">>h_bkg", selection, "goff")

    if selection_signal:
        t_sig.Draw(branch+">>h_sig", selection_signal, "goff")
    else:
        t_sig.Draw(branch+">>h_sig", selection, "goff")

    if t_bkg_test:
        t_bkg_test.Draw(branch+">>h_bkg_test", selection, "goff")

    if t_sig_test:
        if selection_signal:
            t_sig_test.Draw(branch+">>h_sig_test", selection_signal, "goff")
        else:
            t_sig_test.Draw(branch+">>h_sig_test", selection, "goff")

    # Save integrals, used for ROC curves
    total_sig = h_sig.Integral()
    total_bkg = h_bkg.Integral()

    if t_bkg_test:
        total_bkg_test = h_bkg_test.Integral()
    if t_sig_test:
        total_sig_test = h_sig_test.Integral()


    print("Computing ROC Curve")

    # Counters for cumulated number of events
    bkg_n = 0
    sig_n = 0
    bkg_test_n = 0
    sig_test_n = 0
    
    # Arrays containing signal efficiencies and background rejections for TGraphs
    eff_sig, rej_bkg, eff_sig_test, rej_bkg_test = array( 'd' ), array( 'd' ), array( 'd' ), array( 'd' )

    for i in range(nbins, 0, -1):
        bkg_n += h_bkg.GetBinContent(i)
        sig_n += h_sig.GetBinContent(i)
        
        eff_sig.append(sig_n/(1.*total_sig))
        rej_bkg.append(1. - bkg_n/(1.*total_bkg))

        if t_bkg_test and t_sig_test:
            bkg_test_n += h_bkg_test.GetBinContent(i)
            sig_test_n += h_sig_test.GetBinContent(i)

            eff_sig_test.append(sig_test_n/(1.*total_sig_test))
            rej_bkg_test.append(1. - bkg_test_n/(1.*total_bkg_test))

    ROC_curve = ROOT.TGraph(nbins, eff_sig, rej_bkg)
        
    c2 = ROOT.TCanvas()
    c2.SetGrid()
    
    ROC_curve.Draw("AL")
    ROC_curve.SetLineWidth(3)
    ROC_curve.SetLineColor(ROOT.kRed+1)
    ROC_curve.SetLineStyle(1)
    ROC_curve.GetYaxis().SetRangeUser(0.,1.05)
    ROC_curve.GetYaxis().SetTitle("Background Rejection")
    ROC_curve.GetXaxis().SetRangeUser(0.,1.05)
    ROC_curve.GetXaxis().SetTitle("Signal Efficiency")
    ROC_curve.SetTitle("Background rejection vs Signal efficiency")
    
    if t_bkg_test and t_sig_test:
        ROC_curve_test = ROOT.TGraph(nbins, eff_sig_test, rej_bkg_test)
        ROC_curve_test.Draw("L")
        ROC_curve_test.SetLineWidth(3)
        ROC_curve_test.SetLineColor(ROOT.kRed+1)
        ROC_curve_test.SetLineStyle(2)
        
    legend_ROC = ROOT.TLegend(0.1,0.1, 0.5, 0.3)
    legend_ROC.AddEntry(ROC_curve, "ROC Curve for Training Sample")
    if t_bkg_test and t_sig_test:
        legend_ROC.AddEntry(ROC_curve_test, "ROC Curve for Test Sample")
        legend_ROC.Draw()
        
    c2.SaveAs("ROC_curve.png")

    return
