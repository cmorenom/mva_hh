import glob
import logging
import os
import re

import h5py as h5
import numpy as np
import scipy.special
import sklearn.metrics
import uproot


def draw_geometrically(vmin, vmax):
    """ Draw integer in log-space """
    return int(draw_exponentially(vmin, vmax))


def draw_exponentially(vmin, vmax):
    """ Draw float in log-space """
    return np.exp(np.random.uniform(np.log(vmin), np.log(vmax)))


def expand_files(files):
    """ Glob a list of files """
    files_ = []
    for path in files:
        if '*' in path:
            glb = glob.glob(path)
            if glb:
                files_ += glb
            else:
                logging.warning('Expression {} globbed to nothing'.format(path))
        else:
            files_.append(path)
    for path in files_:
        if not os.path.exists(path):
            logging.warning("file {} not found".format(path))
    return files_


def get_bkg_names(files):
    keys = []
    for p in [os.path.basename(f) for f in files]:
        if p.startswith('Bkg_'):
            fields = p.split('_')
            if fields[1] in ['W', 'Z']:
                keys.append('_'.join(fields[1:3]))
            else:
                keys.append(fields[1])
    return set(keys)

def get_parameters(files, groups_sig, dtype=np.float32):
    # ensure we have a list
    if isinstance(groups_sig, str):
        groups_sig = [groups_sig]

    parameters = []

    # First get any explicit parameters, <isGtt>_<MG>_<ML> (for hh: <0>_<mZ'>_<ms>)
    for f in files:
        m = re.match('^([01])_([0-9]+)_([0-9]+)$', f)
        if m:
            parameters.append([
                np.float32(m.group(1)),
                np.float32(m.group(2)),
                np.float32(m.group(3))
            ])

    # check to see if we were instead given a list
    if len(files) == 1 and files[0].endswith('.list'):
        with open(files[0], 'r') as p_file:
            for line in p_file.readlines():
                if not line.startswith('#'):
                    fields = [np.float32(f) for f in line.strip().split()]
                    if len(fields) != 3:
                        raise RuntimeError("malformed parameter: %s", line.strip())
                    # chiara: this might need to be changed
                    if fields[0] == 1 and 'Gtt' in groups_sig or \
                       fields[0] == 0 and 'GGMH' in groups_sig:
                        parameters.append(fields)
    else: # files from which to get parameters
        # get paths to signal files
        paths = [f for f in files if 'GGMH' in os.path.basename(f)]
        # get keys matching groups_sig
        keys = []
        for p in paths:
            tdir = uproot.open(p)
            keys += [k.decode() for k in list(tdir.keys()) if any([g in k.decode() for g in groups_sig])]
        # get the parameters
        for k in keys:
            isGtt = np.float32(k.startswith('Gtt'))
            m = re.match('GGMH_([0-9]*)', k)
            if m:
                parameters.append([isGtt, np.float32(m.group(1)), 0])
    print('parameters: ', np.unique(parameters, axis=0))
    return np.unique(parameters, axis=0).astype(dtype)


def get_scores_and_weights(input_paths, output_paths, keys, variable, index,
                           labels=False, met=None):

    # get selections first
    nbsel = {}
    for key in keys:
        for ipath in input_paths:
            itdir = uproot.open(ipath)
            if not any(k.startswith(key) for k in list(itdir.keys())):
                continue
            sel = itdir[key].array('nb') >= 3
            if met:
                sel = np.logical_and(sel, itdir[key].array('met_mod') > met)
            nbsel[(key, ipath)] = np.where(sel)[0]

    # create output objects
    size = np.sum([nbsel[k].shape[0] for k in nbsel])
    scores = np.zeros((size,), dtype=np.float32)
    weights = np.zeros((size,), dtype=np.float32)
    if labels:
        labels_ = np.zeros((size,), dtype=np.float32)

    # now fill them
    istart = 0
    istop = None
    for key in keys:
        for ipath, opath in zip(input_paths, output_paths):
            if not (key, ipath) in nbsel:
                continue
            itdir = uproot.open(ipath)
            data = itdir[key].arrays(['weight', 'weight_lumi_real'])

            with h5.File(opath, 'r') as ofile:
                scores_ = ofile[key][variable][:, index]

            istop = istart + nbsel[(key, ipath)].shape[0]
            scores[istart:istop] = scores_[nbsel[(key, ipath)]]
            weights[istart:istop] = data['weight'][nbsel[(key, ipath)]] \
                                  * data['weight_lumi_real'][nbsel[(key, ipath)]]

            if labels:
                labels_[istart:istop] = 1 if os.path.basename(ipath).startswith('Sig_') else 0

            istart = istop

    if labels:
        return scores, weights, labels_
    else:
        return scores, weights


def _match_input(path, fold):
    case1 = re.search('NNinput\.{}$'.format(fold), path)
    case2 = re.search('NNinput\.[0-9]+\.{}$'.format(fold), path)
    return case1 or case2


def group_files(paths, group_sig=None):
    # doesn't work for hh workflow
    '''
    data_input = {
    'training': [p for p in paths if _match_input(p, 'training')],
    'validation': [p for p in paths if _match_input(p, 'validation')],
    'test': [p for p in paths if _match_input(p, 'test')],
    'data': [p for p in paths if '20' in p and p.endswith('NNinput')]
    }
    '''
    data_input = {
        'training': [p for p in paths if '.training' in p and not 'NNoutput' in p],
        'validation': [p for p in paths if '.validation' in p and not 'NNoutput' in p],
        'test': [p for p in paths if '.test' in p and not 'NNoutput' in p],
        'data': [p for p in paths if '20' in p and not 'NNoutput' in p],
        'all': [p for p in paths if '21.2.87-EWK-1-B_mc' in p and not 'NNoutput' in p]
    }
    data_output = {
        'training': [p for p in paths if '.training.NNoutput' in p],
        'validation': [p for p in paths if '.validation.NNoutput' in p],
        'test': [p for p in paths if '.test.NNoutput' in p],
        'data': [p for p in paths if '20' in p and 'NNoutput' in p],
        'all': [p for p in paths if '21.2.87-EWK-1-B_mc' in p and 'NNoutput' in p]
    }
    print('data_input:\n',data_input)
    print('data_output:\n',data_output)

    # now enforce the order
    for fold in ['training', 'validation', 'test', 'data','all']:
        tmp = []
        for bpath in [os.path.basename(p) for p in data_input[fold]]:
            if fold == 'data':
                bpath = bpath.replace('.NNinput', '')
            expect = '{}.BDToutput{}.h5'.format(bpath, '' if not group_sig else ('-' + group_sig))
#            expect = '{}.NNoutput{}.h5'.format(bpath, '' if not group_sig else ('-' + group_sig))
            try:
                i = [os.path.basename(p) for p in data_output[fold]].index(
                    expect
                )
            except ValueError:
                raise RuntimeError('Missing NNoutput for input %s' % bpath)
            tmp.append(data_output[fold][i])
        # TODO raise error if tmp is not same as data_output[fold] modulo order
        data_output[fold] = tmp
    print('data_input:\n',data_input)
    print('data_output:\n',data_output)
    return data_input, data_output


def significance(signalExp, backgroundExp, relativeBkgUncert):
    """ Numpy/Scipy port of the RooStats function `BinomialExpZ'

    See: https://root.cern.ch/doc/master/NumberCountingUtils_8cxx_source.html
    """
    # pylint: disable=invalid-name
    mainInf = signalExp + backgroundExp
    tau = 1.0 / backgroundExp / (relativeBkgUncert * relativeBkgUncert)
    auxiliaryInf = backgroundExp * tau
    P_Bi = scipy.special.betainc(mainInf, auxiliaryInf + 1, 1.0 / (1.0 + tau))
    return - scipy.special.ndtri(P_Bi)
