""" Code to train/evaluated a network """

import json
import logging

#import keras
import numpy as np

from mbj_nn import callbacks


def setup_training(hps, data, output_path, restart=None):
    """ Prepare to train a network

    * build and compile a keras model from the json config
    * compute and apply normalization to training input
    * shuffle the dataset
    * setup option dict for keras.Model.fit method
      * Notably, adds TerminateOnNaN, ModelCheckpoint & EarlyStopping callbacks

    Arguments:
    hps -- hyperparameter dict
    data -- (data_x, data_y, data_w) to compute shape, normalization, etc
    restart -- optional, tuple (path_to_weights, epoch, baseline validation)
               to be used to restart training

    Returns: (keras model, fit options, normalization)
    """

    if len(data) == 3:
        data_x, data_y, data_w = data
        data_w_tr = None
    elif len(data) == 4:
        data_x, data_y, data_w, data_w_tr = data

    k_model = _build(hps, data_x.shape[1], data_y.shape[1])

    fitcfg = {}

    fitcfg['batch_size'] = hps.get('batch_size', 32)
    fitcfg['epochs'] = hps.get('epochs', 100)
    fitcfg['verbose'] = 2
    fitcfg['shuffle'] = True
    if hps.get('use_weights'):
        # We don't use the physical weights for training anymore, just for
        # validation
        pass

    if restart is not None:
        restart_weights, restart_epoch, restart_baseline = restart
        k_model.load_weights(restart_weights)
        fitcfg['initial_epoch'] = restart_epoch
        logging.info(
            'restarting training at epoch=%d, with baseline=%f, with weights from %s',  # noqa
            restart_epoch,
            restart_baseline,
            restart_weights
        )
    else:
        restart_baseline = None
    # chiara: ?
    fitcfg['callbacks'] = _setup_callbacks(hps, output_path, restart_baseline)

    ishuf = np.arange(data_x.shape[0])
    np.random.shuffle(ishuf)
    data_x = data_x[ishuf]
    data_y = data_y[ishuf]
    data_w = data_w[ishuf]
    if data_w_tr is not None:
        data_w_tr = data_w_tr[ishuf]

    ntrain = int(data_x.shape[0] * (1 - hps.get('validation_split', 0.01)))
    fitcfg['x'] = data_x[:ntrain]
    fitcfg['y'] = data_y[:ntrain]
    if data_w_tr is not None:
        fitcfg['sample_weight'] = data_w_tr[:ntrain]

    norm = {
        'mean': np.mean(fitcfg['x'], axis=0),
        'std': np.std(fitcfg['x'], axis=0),
    }
    norm['std'][np.where(norm['std'] == 0)] = 1

    fitcfg['x'] -= norm['mean']
    fitcfg['x'] /= norm['std']

    fitcfg['validation_data'] = (
        (data_x[ntrain:] - norm['mean']) / norm['std'],
        data_y[ntrain:]
    )
    if hps.get('use_weights'):
        fitcfg['validation_data'] = fitcfg['validation_data'] + (data_w[ntrain:],)

    return k_model, fitcfg, norm


def _build(hps, n_input, n_output):

    logging.info('Building model structure')

    input_node = keras.layers.Input((n_input,))
    k_model = input_node

    if 'dropout_input' in hps and hps['dropout_input'] > 0:
        k_model = keras.layers.Dropout(hps['dropout_input'])(k_model)

    complex_reg = ['hidden_l1', 'output_l1', 'hidden_l2', 'output_l2']
    if all([k in hps for k in complex_reg]):
        hidden_reg = (hps['hidden_l1'], hps['hidden_l2'])
        output_reg = (hps['output_l1'], hps['output_l2'])
    else:
        hidden_reg = (0.0, hps['l2'])
        output_reg = (0.0, hps['l2'])

    for _ in range(hps['n_hidden_layers']):
        k_model = keras.layers.Dense(
            hps['n_hidden_units'],
            kernel_regularizer=keras.regularizers.l1_l2(
                l1=hidden_reg[0],
                l2=hidden_reg[1]
            ),
            use_bias=(('batchnorm' not in hps) or (not hps['batchnorm']))
        )(k_model)

        if 'batchnorm' in hps and hps['batchnorm']:
            k_model = keras.layers.BatchNormalization()(k_model)

        k_model = keras.layers.Activation('relu')(k_model)

        if 'dropout_hidden' in hps and hps['dropout_hidden'] > 0:
            k_model = keras.layers.Dropout(hps['dropout_hidden'])(k_model)

    output_node = keras.layers.Dense(
        n_output,
        kernel_regularizer=keras.regularizers.l1_l2(
            l1=output_reg[0],
            l2=output_reg[1],
        ),
        activation='softmax'
    )(k_model)

    k_model = keras.models.Model(inputs=input_node, outputs=output_node)

    k_model.compile(
        optimizer=keras.optimizers.Adam(lr=hps['learning_rate']),
        loss='categorical_crossentropy',
    )

    return k_model


def _setup_callbacks(hps, output_path, restart_baseline):

    my_callbacks = [
        keras.callbacks.TerminateOnNaN(),
    ]

    if hps.get('earlystopping_fscore'):
        my_callbacks.append(
            callbacks.EarlyStopping_fscore(
                nepochs=hps.get('earlystopping_epochs', 20),
                checkpoint=True,
                output=output_path,
                baseline=restart_baseline
            )
        )
    else:
        my_callbacks += [
            keras.callbacks.EarlyStopping(
                patience=hps.get('earlystopping_epochs', 20),
                verbose=1,
                baseline=restart_baseline
            ),
            keras.callbacks.ModelCheckpoint(
                filepath=output_path,
                save_best_only=True,
                verbose=1,
            )
        ]
        if restart_baseline:
            my_callbacks[-1].best = restart_baseline

    return my_callbacks

