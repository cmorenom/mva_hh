""" Functions to load the mbj dataset for NN training or evaluation """
import argparse
import collections
import json
import logging
import re
import resource

import numpy as np
import uproot
from sklearn.utils import shuffle


# TODO think of better way to do this...
def _gtt_scale_factor(mg, ml):
    if (mg <= 2000) and ((mg - ml) <= 500):
        return 4.0 / 8.0
    if (mg == 1100) and (ml in [1, 200, 400]):
        return 4.0 / 7.0
    if mg >= 2500:
        return 4.0 / 3.0
    return 1.0


def read_branches(path):
    """ Read branch list from a file (one branch per line) """
    with open(path, 'r') as brfile:
        return [b.strip() for b in brfile.readlines()]


def read_group_to_target(path):
    """ Read NN target number for groups from json file """
    with open(path, 'r') as tgtfile:
        return json.load(tgtfile)

def get_branch_mask (branches, b, sel, bb = None):
    if not bb:
        if sel[b][0].strip() == '>':
            return branches[b] > sel[b][1]
        if sel[b][0].strip() == '>=':
            return branches[b] >= sel[b][1]
        if sel[b][0].strip() == '<':
            return branches[b] < sel[b][1]
        if sel[b][0].strip() == '<=':
            return branches[b] <= sel[b][1]
        if sel[b][0].strip() == '==':
            return branches[b] == sel[b][1]
    else:
        if sel[b][bb][0].strip() == '>':
            return branches[b] > sel[b][bb][1]
        if sel[b][bb][0].strip() == '>=':
            return branches[b] >= sel[b][bb][1]
        if sel[b][bb][0].strip() == '<':
            return branches[b] < sel[b][bb][1]
        if sel[b][bb][0].strip() == '<=':
            return branches[b] <= sel[b][bb][1]
        if sel[b][bb][0].strip() == '==':
            return branches[b] == sel[b][bb][1]
        
    print('error!')
    return 0

def get_mask(tree, sel_list):
    mask = None
    isel = 0
    for sel in sel_list:
        b_list = list(sel.keys())
        branches = tree.arrays(b_list, namedecode='utf-8')
        for b in b_list:
            if isinstance(sel[b], list):
                if isel == 0:
                    mask = get_branch_mask (branches, b, sel)
                else: mask = mask & (get_branch_mask (branches, b, sel))
                isel += 1
            elif isinstance(sel[b], dict):
                bb_list = list(sel[b].keys())
                for bb in bb_list:
                    mask = mask & (get_branch_mask(branches, b, sel, bb))
                    isel += 1
    return mask
            

def get_mask_dict(tdir, keys, sel=None, sel_sig=None):        
    mask_dict = {}
    if not sel and not sel_sig:
        for key in keys:
            mask_dict[key] = np.ones(tdir[key].numentries, dtype=bool)
    else:
        sel_list = []
        if sel: sel_list.append(sel)
        for key in keys:
            sel_list_use = sel_list
            # if signal, apply also signal-specific selections
            match = re.match('GMMH_([0-9]*)', key) 
            if sel_sig and match: sel_list_use.append(sel_sig) 
            mask_dict[key] = get_mask(tdir[key], sel_list)
    return mask_dict
        
def get_numentries_dict(tdir, keys=None, sel=None, sel_sig=None, mask_dict=None):
    if not keys:
        keys = set([k.decode().split(';')[0] for k in tdir.keys() ])#if b'_nominal' in k])
    numentries_dict = {}
    if not mask_dict:
        mask_dict = get_mask_dict(tdir, keys, sel, sel_sig)
    for key in keys:
        numentries_dict[key] = mask_dict[key].sum()
    return numentries_dict

def load(paths, branches, group_to_target, hps={}, sel=None, sel_sig=None, evBkg=None):
    """ Load the mbj dataset

    Mainly useful for training, as all data is concatenated in single array
    """

    # Start by computing the array size
    nevents = 0
    for path in paths:
        tdir = uproot.open(path)
        print('get entries')
        keys = set([k.decode().split(';')[0] for k in tdir.keys()]) # if b'_nominal' in k])        
        print(keys)
        numentries_dict = get_numentries_dict(tdir, keys=keys, sel=sel, sel_sig=sel_sig)
        #keys = set([k.split(';')[0] for k in tdir.keys(namedecode='utf-8') if '_nominal' in k])        
        for key in keys:
            groups = [ k for k in group_to_target ]
            group = '_'.join(key.split('_')[:-1]) # remove the syst tree suffix
            if group not in groups: continue
            logging.info(
                'Loading {} entries from {}:{}'.format(
                    numentries_dict[key], # chiara: this needs to be changed as well
                    path,
                    key
                )
            )
            nevents += numentries_dict[key]

    # Create the target arrays

    data_x = np.zeros((nevents, len(branches)+1), dtype=np.float32) 
    data_y = np.zeros(
        (nevents, np.max(list(group_to_target.values())) + 1),
        dtype=np.float32
    )
    data_w = np.zeros((nevents,))

    istart = 0
    parameters = collections.defaultdict(int) # Create dictionary for higgsino mass distribution

    print("Loading signals")
    for path in paths:
        # load signals first
        istart = _load_internal(
            uproot.open(path),
            data_x,
            data_y,
            data_w,
            branches,
            group_to_target,
            parameters,
            istart,
            do_signal=True,  # Only load signal trees
            sel=sel,
            sel_sig=sel_sig
        )

    evNumber = istart

    # freeze the parameters dict
    parameters.default_factory = None # chiara: ?

    print("Loading Backgrounds")

    for path in paths:
        # Now do the background
        istart = _load_internal(
            uproot.open(path),
            data_x,
            data_y,
            data_w,
            branches,
            group_to_target,
            parameters,
            istart,
            do_signal=False,  # Only load background trees
            sel=sel
        )

    if istart != data_x.shape[0]:
        raise RuntimeError("Part of the dataset left unfilled!")


    if evBkg: # reshuffle background and keep just evBkg events
        logging.info("Keeping only %s background events", evBkg)
        print("Ev number: ", evNumber)

        data_x_sig = data_x[:evNumber]
        data_y_sig = data_y[:evNumber]
        data_w_sig = data_w[:evNumber]

        data_x_bkg = data_x[evNumber+1:]
        data_y_bkg = data_y[evNumber+1:]
        data_w_bkg = data_w[evNumber+1:]

        data_x_bkg, data_y_bkg, data_w_bkg = shuffle(data_x_bkg, data_y_bkg, data_w_bkg, random_state=0)
        data_x_bkg = data_x_bkg[:evBkg]
        data_y_bkg = data_y_bkg[:evBkg]
        data_w_bkg = data_w_bkg[:evBkg]

        data_x = np.concatenate((data_x_sig, data_x_bkg))
        data_y = np.concatenate((data_y_sig, data_y_bkg))
        data_w = np.concatenate((data_w_sig, data_w_bkg))

    return data_x, data_y, data_w


def itertrees(path, branches, keys=None, allowed=None, nominal_only=False):
    """ iterate over trees in the file

    As of now, this only loads the NN input, so this is mainly
    useful for evaluating the NN while preserving the file
    structure
    """

    tdir = uproot.open(path)
    if not keys:
        keys = set([k.decode().split(';')[0] for k in tdir.keys()])
        if 'cut_flow' in keys:
            keys.remove('cut_flow')

    if allowed is not None: # None actually means that everything is allowed
        keys = [k for k in keys if k in allowed]

    if nominal_only:
        keys = [k for k in keys if k.endswith('_nominal')]

    for key in keys:
        data_x = np.zeros(
            (tdir[key].numentries, len(branches) + 1),
            dtype=np.float32
        )
        for i, brn in enumerate(branches):
            this_var = tdir[key].array(brn)
            data_x[:, i] = this_var
        yield key, data_x


def set_parameters(data_x, mhiggsino, normalization=None):

    data_x[:, -1] = mhiggsino


def _load_internal(tdir, data_x, data_y, data_w, branches, group_to_target,
                   parameters, istart, do_signal, sel=None,  sel_sig=None):
    # remember: tdir : uproot.open(path)

    if not do_signal: 
        param_idx = np.array(list(parameters.keys())) #Contains mass points
        param_probs = np.array(list(parameters.values())).astype(np.float32) \
            / np.sum(list(parameters.values())) # Contains the probability for each mass point

    # keys: list of tree names in file
    keys = set([k.decode().split(';')[0] for k in tdir.keys()]) # if b'_nominal' in k])
    mask_dict = get_mask_dict(tdir, keys=keys, sel=sel, sel_sig=sel_sig)

    numentries_dict = get_numentries_dict(tdir, keys=keys, sel=sel, sel_sig=sel_sig, mask_dict=mask_dict)

    for key in keys:
        match = re.match('GGMH_([0-9]*)', key) # chiara: changed!  
        if match:
            # We found a signal tree
            if do_signal:
                # We want to process it
                mhiggsino = np.float32(match.group(1))
                print('mhiggsino ', mhiggsino)# --> checl I'm isolating the correct numbers
                parameters[mhiggsino] += numentries_dict[key]
            else:
                continue
        else:
            # We found a background tree, skip it we're looking for signal
            if do_signal:
                continue

        groups = [ k for k in group_to_target ]
        print(groups)

        if key == 'tree':
            group = 'tree'
        else:
            group = '_'.join(key.split('_')[:-1])  # remove the syst tree suffix

        if group not in groups: # ignore files that are not in the group_to_target dict
            del parameters[mhiggsino] # remove the parameter from the parameters list
            continue

        # If we get here, we load the data
        istop = istart + numentries_dict[key]
        '''
        # old version, problematic for hh because you need to know the data type in advance 
        for i, brn in enumerate(branches):            
            # print(brn)
            tdir[key].arrays(
                # chiara" changed ">f8" to ">f4"
                {brn: uproot.asarray(">f4", data_x[istart:istop, i])}#, 
                #namedecode='utf-8'
            )
            print(brn)
            print(data_x[istart:istart+5, i])
            print(data_x[istop-5:istop, i],'\n')
            '''
        # using pd df allows to read in one go without specofying data type
        data_x[istart:istop, 0:len(branches)] = tdir[key].pandas.df(branches)[mask_dict[key]].to_numpy()
        if do_signal:
            set_parameters(data_x[istart:istop], mhiggsino)
        else:
            # parametrize the background here
            iparam = np.random.choice(param_idx, p=param_probs, size=numentries_dict[key])

            data_x[istart:istop,-1:] = np.reshape(iparam, (iparam.size,1))
            
        data_y[istart:istop, group_to_target[group]] = 1  # one-hot encoding
        # chiara: it was "weight" and not "weight_mc", and "weight_lumi_real" but I have only this atm
        # Carlos: Add all the weights, MET trigger SFs could affect the training too, and will be needed for dijet reweighted samples
        if do_signal:
            data_w[istart:istop] = 1.0
        else:
            data_w[istart:istop] = tdir[key].array('weight_mc')[mask_dict[key]] * tdir[key].array('weight_lumi')[mask_dict[key]] * tdir[key].array('weight_met_trig')[mask_dict[key]]

        istart = istop

    return istart


if __name__ == '__main__':
    # pylint: disable=invalid-name
    args = argparse.ArgumentParser()
    args.add_argument('--files', nargs='+', required=True)
    args.add_argument('--branches', metavar='branches.list', required=True)
    args.add_argument(
        '--group-to-target',
        metavar='group_to_target.json',
        required=True
    )
    args.add_argument('--balance-signal', action='store_true')
    args = args.parse_args()

    data = load(
        args.files,
        read_branches(args.branches),
        read_group_to_target(args.group_to_target),
        hps={'balance_signal': args.balance_signal}
    )

    print(
        'maxrss: {} kB'.format(
            resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        )
    )
