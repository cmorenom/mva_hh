""" custom keras callbacks """
import logging

import h5py as h5
import numpy as np
import keras


class EarlyStopping_fscore(keras.callbacks.Callback):
    """ early stop with fscore metric """

    def __init__(self,
                 nepochs=10,
                 output_bin=0,
                 checkpoint=None,
                 output=None,
                 baseline=0.0,
                 **kwargs):
        self.nepochs = nepochs
        self.remain = self.nepochs
        self.output_bin = output_bin
        self.best = baseline if baseline else 0.0
        self.checkpoint = checkpoint
        self.output_path = output
        self.history = []
        self.saved = False

        self.weights = None
        self.y_true = None
        self.tsel = None
        self.fsel = None
        self.npos = None

        super(EarlyStopping_fscore, self).__init__(**kwargs)

    def on_train_begin(self, logs=None):
        self.model.save(self.output_path)
        logging.info('initial model saved to %s', self.output_path)

    def on_train_end(self, logs=None):
        if self.output_path:
            with h5.File(self.output_path, 'a') as wfile:
                wfile.create_dataset('fscores', data=np.array(self.history))
                logging.info('fscore history saved to %s', self.output_path)

    def on_epoch_end(self, epoch, logs=None):
        # pylint: disable=unsubscriptable-object

        if self.weights is None:
            self.weights = self.validation_data[2]
            if self.weights is None:
                self.weights = np.ones(self.validation_data[0].shape[0])

        if self.y_true is None:
            self.y_true = self.validation_data[1][:, self.output_bin]
            self.tsel = self.y_true == 1
            self.fsel = self.y_true == 0
            self.npos = np.sum(self.weights[np.where(self.tsel)])

        y_pred = (np.argmax(
            self.model.predict(self.validation_data[0]),
            axis=-1
        ) == self.output_bin).astype(np.float32)

        predsel = y_pred == 1
        tpsel = np.where(np.logical_and(predsel, self.tsel))
        fpsel = np.where(np.logical_and(predsel, self.fsel))

        tp = np.sum(self.weights[tpsel])
        fp = np.sum(self.weights[fpsel])

        precision = tp / (tp + fp)
        recall = tp / self.npos

        if precision + recall == 0:
            fscore = 0
        else:
            fscore = 2 * precision * recall / (precision + recall)

        self.history.append(fscore)

        if fscore > self.best:
            logging.info(
                'epoch %05d: fscore increased %.5f -> %.5f',
                epoch,
                self.best,
                fscore
            )
            self.best = fscore
            self.remain = self.nepochs

            if self.checkpoint:
                self.model.save(self.output_path)
                self.saved = True
                logging.info('model saved to %s', self.output_path)
        else:
            logging.info(
                'epoch %05d: fscore not increased %.5f -> %.5f',
                epoch,
                self.best,
                fscore
            )
            self.remain -= 1
        logging.info(
            'epoch %05d: %d epochs with non-increasing fscore remaining before early stopping',
            epoch,
            self.remain
        )

        if self.remain == 0:
            logging.info('epoch %05d: early stopping', epoch)
            self.model.stop_training = True
