CXXFLAGS = -O2 -Wall -Wextra -pedantic $(shell root-config --auxcflags)
INCS = -isystem $(shell root-config --incdir) -Iinclude
LIBS = $(shell root-config --libs) -lstdc++fs

targets: bin/splitTrees bin/makeNNinput bin/reweight bin/weight_NN

.SECONDARY: $(patsubt bin/%,obj/%.o,$(targets))

bin/%: obj/%.o obj/utils.o | bin
	g++ $(CXXFLAGS) $(INCS) -o $@ $^ $(LIBS)

obj/%.o: src/%.cxx | obj
	g++ $(CXXFLAGS) $(INCS) -c -o $@ $<

obj/%.o: src/%.cxx include/%.h | obj
	g++ $(CXXFLAGS) $(INCS) -c -o $@ $<

obj:
	mkdir -p obj

bin:
	mkdir -p bin

clean:
	rm -rf bin obj
	rm -f scripts/*.pyc
