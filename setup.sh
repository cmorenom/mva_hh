DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

export PATH=$DIR/bin:$DIR/scripts:$PATH
export PYTHONPATH=$DIR/python:$PYTHONPATH
