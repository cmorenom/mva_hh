import argparse

import numpy as np
import uproot

import mbj_nn.dataset

# configuration
args = argparse.ArgumentParser()
args.add_argument('source', nargs='+')
args = args.parse_args()

# run it
data_x, data_y, data_w, data_tr = mbj_nn.dataset.load(
    args.source,
    mbj_nn.dataset.read_branches('tests/branches.list'),
    mbj_nn.dataset.read_group_to_target('tests/group_to_target.json'),
    hps={'balance_signal': True}
)

np.testing.assert_equal(data_x.shape[0], data_y.shape[0])
np.testing.assert_equal(data_x.shape[0], data_w.shape[0])
np.testing.assert_equal(data_x.shape[0], data_tr.shape[0])

for i, br in enumerate(mbj_nn.dataset.read_branches('tests/branches.list')):
    istart = 0
    for path in args.source:
        source = uproot.open(path)
        for key in set([k.split(';')[0] for k in source.keys() if '_nominal' in k]):
            istop = istart + source[key].numentries
            np.testing.assert_allclose(source[key].array(br), data_x[istart:istop, i])
            istart = istop

np.testing.assert_array_equal(data_tr[np.where(data_y[:, 0] == 0)], 1)
np.testing.assert_allclose(
    data_tr[np.where(np.logical_and(data_x[:, -2] >= 2500, data_y[:, 0] == 1))],
    4.0 / 3.0
)
np.testing.assert_allclose(np.unique(data_tr), 4.0 / np.array([8, 7, 4, 3]))

# run it and now balance s vs b
data_x, data_y, data_w, data_tr = mbj_nn.dataset.load(
    args.source,
    mbj_nn.dataset.read_branches('tests/branches.list'),
    mbj_nn.dataset.read_group_to_target('tests/group_to_target.json'),
    hps={'balance_s_vs_b': True}
)

ssum = np.count_nonzero(data_y[:, 0])
bsum = np.count_nonzero(1 - data_y[:, 0])
np.testing.assert_allclose(np.sum(data_tr[np.where(data_y[:, 0] == 1)]), bsum)
np.testing.assert_allclose(np.sum(data_tr[np.where(data_y[:, 0] == 0)]), bsum)

# same with both weighting scheme at once
data_x, data_y, data_w, data_tr = mbj_nn.dataset.load(
    args.source,
    mbj_nn.dataset.read_branches('tests/branches.list'),
    mbj_nn.dataset.read_group_to_target('tests/group_to_target.json'),
    hps={'balance_signal': True, 'balance_s_vs_b': True}
)

ssel = np.where(data_y[:, 0] == 1)
ssum = np.sum(data_tr[ssel])
bsel = np.where(data_y[:, 0] == 0)
bsum = np.sum(data_tr[bsel])
np.testing.assert_allclose(ssum, bsum, rtol=1e-3)

