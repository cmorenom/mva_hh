import argparse
import os
import subprocess

scriptdir = os.path.dirname(os.path.realpath(__file__))

args = argparse.ArgumentParser()
args.add_argument('source')
args = args.parse_args()


subprocess.check_call([
    'trainNN.py',
    '--files', args.source,
    '--branches', '{}/branches.list'.format(scriptdir),
    '--group-to-target', '{}/group_to_target.json'.format(scriptdir),
    '--model', '{}/model.json'.format(scriptdir),
    '--max-epoch', '1',
    '--seed', '3432343'
])

subprocess.check_call([
    'evalNN.py',
    '--files', args.source,
    '--branches', '{}/branches.list'.format(scriptdir),
    '--model', 'trained_model.h5',
    '--masses', '1100,1', '1100,200'
])
