import argparse
import os
import subprocess


import numpy as np
import ROOT
import root_numpy


# configuration
args = argparse.ArgumentParser()
args.add_argument('source')
args = args.parse_args()

# run it
subprocess.check_call(
    "splitTrees {} 0.5 0.25 0.25 2112".format(args.source),
    shell=True
)

# files exist?
train_path = os.path.basename(args.source + '.training')
assert os.path.exists(train_path)
valid_path = os.path.basename(args.source + '.validation')
assert os.path.exists(valid_path)
test_path = os.path.basename(args.source + '.test')
assert os.path.exists(test_path)

# test tree presence
source = ROOT.TFile(args.source, 'READ')
train = ROOT.TFile(train_path, 'READ')
valid = ROOT.TFile(valid_path, 'READ')
test = ROOT.TFile(test_path, 'READ')

for tree in [k.GetName() for k in source.GetListOfKeys()]:
    assert tree in [k.GetName() for k in train.GetListOfKeys()]
    assert tree in [k.GetName() for k in valid.GetListOfKeys()]
    assert tree in [k.GetName() for k in test.GetListOfKeys()]

# test number of events
for treename in [k.GetName() for k in source.GetListOfKeys()]:
    source_tree = source.Get(treename)
    train_tree = train.Get(treename)
    valid_tree = valid.Get(treename)
    test_tree = test.Get(treename)
    nevent = source_tree.GetEntries()
    assert nevent == train_tree.GetEntries() + valid_tree.GetEntries() + test_tree.GetEntries()
    np.testing.assert_allclose(0.5*nevent, train_tree.GetEntries(), rtol=1e-1)
    np.testing.assert_allclose(0.25*nevent, valid_tree.GetEntries(), rtol=1e-1)
    np.testing.assert_allclose(0.25*nevent, test_tree.GetEntries(), rtol=1e-1)

# TODO when have file with >1 syst variation, test that each syst
# variation for same prefix have same number of events

# test for duplicates with arbitrary branches, unlikely to all
# be equal for 2 different events. this is because it's tough to work
# with numpy structured arrays with variable length fields
branches = ['event_number', 'process', 'mettst', 'meff_incl', 'dphi_min']
for treename in [k.GetName() for k in source.GetListOfKeys()]:
    source_array = root_numpy.root2array(args.source, treename, branches)
    train_array = root_numpy.root2array(train_path, treename, branches)
    valid_array = root_numpy.root2array(valid_path, treename, branches)
    test_array = root_numpy.root2array(test_path, treename, branches)
    n_unique = np.unique(
        np.concatenate((train_array, valid_array, test_array))
    ).shape[0]
    assert source_array.shape[0] == n_unique
