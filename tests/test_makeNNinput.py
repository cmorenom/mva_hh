import argparse
import os
import subprocess

import numpy as np
import ROOT
import root_numpy


# configuration
args = argparse.ArgumentParser()
args.add_argument('source')
args = args.parse_args()

nninput_path = os.path.basename(args.source) + '.NNinput'

# first, run it on allpass mode
subprocess.check_call(
    "makeNNinput {} {} 10 4 4 1 0 0".format(args.source, nninput_path),
    shell=True
)

# files exist?
assert os.path.exists(args.source)
assert os.path.exists(nninput_path)

# test tree presence
source = ROOT.TFile(args.source, 'READ')
nninput = ROOT.TFile(nninput_path, 'READ')
for tree in [k.GetName() for k in source.GetListOfKeys()]:
    assert tree in [k.GetName() for k in nninput.GetListOfKeys()]

# test number of events
for treename in [k.GetName() for k in source.GetListOfKeys()]:
    source_tree = source.Get(treename)
    nninput_tree = nninput.Get(treename)
    assert source_tree.GetEntries() == nninput_tree.GetEntries()

# check branches presence
branches = []
for i in range(10):
    for var in ['pt', 'eta', 'phi', 'm', 'isb']:
        branches.append('small_R_jets_{}_{}'.format(var, i))
for i in range(4):
    for var in ['pt', 'eta', 'phi', 'm']:
        branches.append('large_R_jets_{}_{}'.format(var, i))
for i in range(4):
    for var in ['pt', 'eta', 'phi', 'm']:
        branches.append('leptons_{}_{}'.format(var, i))
branches += ['met_mod', 'met_phi']
for treename in [k.GetName() for k in source.GetListOfKeys()]:
    nninput_tree = nninput.Get(treename)
    nninput_br = [b.GetName() for b in nninput_tree.GetListOfBranches()]
    for b in branches:
        assert b in nninput_br, "branch {} not found".format(b)

# check equivalence for e.g. met
for tree in [k.GetName() for k in source.GetListOfKeys()]:
    met_src = root_numpy.root2array(
        args.source,
        tree,
        ['mettst', 'mettst_phi']
    )
    met_dst = root_numpy.root2array(
        nninput_path,
        tree,
        ['met_mod', 'met_phi']
    )
    np.testing.assert_array_almost_equal(met_src['mettst'], met_dst['met_mod'])

# Re-run, but with pxpypze and with pre-selection
subprocess.check_call('mv {f} {f}.bak'.format(f=nninput_path), shell=True)
subprocess.check_call(
    "makeNNinput {} {} 10 4 4 0 1 0".format(args.source, nninput_path),
    shell=True
)

# test tree presence
source = ROOT.TFile(args.source, 'READ')
nninput = ROOT.TFile(nninput_path, 'READ')
for tree in [k.GetName() for k in source.GetListOfKeys()]:
    assert tree in [k.GetName() for k in nninput.GetListOfKeys()]

# test number of events
for treename in [k.GetName() for k in source.GetListOfKeys()]:
    source_tree = source.Get(treename)
    nninput_tree = nninput.Get(treename)
    assert source_tree.GetEntries() > nninput_tree.GetEntries()

# check branches presence
branches = []
for i in range(10):
    for var in ['px', 'py', 'px', 'e', 'isb']:
        branches.append('small_R_jets_{}_{}'.format(var, i))
for i in range(4):
    for var in ['px', 'py', 'pz', 'e']:
        branches.append('large_R_jets_{}_{}'.format(var, i))
for i in range(4):
    for var in ['px', 'py', 'pz', 'e']:
        branches.append('leptons_{}_{}'.format(var, i))
branches += ['met_px', 'met_py']
for treename in [k.GetName() for k in source.GetListOfKeys()]:
    nninput_tree = nninput.Get(treename)
    nninput_br = [b.GetName() for b in nninput_tree.GetListOfBranches()]
    for b in branches:
        assert b in nninput_br, "branch {} not found".format(b)

# recover the ptetaphim version, to be used by other tests
subprocess.check_call('mv {f}.bak {f}'.format(f=nninput_path), shell=True)
