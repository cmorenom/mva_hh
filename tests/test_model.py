import argparse
import os

import keras
import numpy as np

import mbj_nn.model

def _gen_data():
    # pylint: disable=no-member
    xdat = np.random.normal(size=(10000, 2)).astype(np.float32)
    ydat = np.logical_xor(
        xdat[:, 0] >= 0,
        xdat[:, 1] >= 0
    ).astype(np.float32)[:, np.newaxis]
    return xdat, keras.utils.to_categorical(ydat)


xdat, ydat = _gen_data()


k_model, fitcfg, norm = mbj_nn.model.setup_training(
    path=os.path.dirname(os.path.realpath(__file__)) + '/model.json',
    data=(xdat, ydat, None),
    output_path='test_model_1.h5',
)

k_model.fit(**fitcfg)
