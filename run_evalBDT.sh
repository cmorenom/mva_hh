#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/diboson_mc16a_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/diboson_mc16d_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/diboson_mc16e_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/singletop_mc16a_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/singletop_mc16d_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/singletop_mc16e_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/topEW_mc16a_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/topEW_mc16d_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/topEW_mc16e_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/ttbar_mc16a_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/ttbar_mc16d_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/ttbar_mc16e_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/W_jets_mc16a_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/W_jets_mc16d_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/W_jets_mc16e_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/Z_jets_mc16a_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/Z_jets_mc16d_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &
#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/Z_jets_mc16e_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &

#nohup python3 scripts/evalBDT.py --files /nfs/at3/scratch/cmorenom/NN_higgsino/samples/test/skimmed/data_skimmed.root.test --branches config/branches_highlevel.list --trained_model BDT_test.json &

nohup python3 scripts/evalBDT.py --files ../samples/test/Sig_21.2.114_mc16a* --branches config/branches_highlevel.list --trained_model models/HPS/1100/BDT_193.json --output ../results/BDT1100/unmerged/ &
nohup python3 scripts/evalBDT.py --files ../samples/test/Sig_21.2.114_mc16d* --branches config/branches_highlevel.list --trained_model models/HPS/1100/BDT_193.json --output ../results/BDT1100/unmerged/ &
nohup python3 scripts/evalBDT.py --files ../samples/test/Sig_21.2.114_mc16e* --branches config/branches_highlevel.list --trained_model models/HPS/1100/BDT_193.json --output ../results/BDT1100/unmerged/ &
