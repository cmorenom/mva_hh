// -*- mode:c++ -*-
#ifndef __UTILS_H__
#define __UTILS_H__

#include <string>
#include <vector>

#include <TFile.h>
#include <TKey.h>

namespace utils {
std::string basename(const std::string &path);
std::vector<TKey*> get_ttree_keys(TFile *tf);
}

#endif // __UTILS_H__
