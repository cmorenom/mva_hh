#include <cstring>

#include "utils.h"

namespace utils {

std::string basename(const std::string &path)
{
	size_t i;
	if ((i = path.find_last_of("/")) != std::string::npos)
		return path.substr(i + 1);
	return std::string(path);
}

std::vector<TKey*> get_ttree_keys(TFile *tf)
{
	std::vector<TKey*> keys;
	TList *lst = tf->GetListOfKeys();
	for (auto it = lst->begin(); it != lst->end(); ++it) {
		TKey *key = static_cast<TKey*>(*it);
		if (std::strcmp(key->GetClassName(), "TTree") == 0)
			keys.push_back(key);
	}
	return keys;
}

}
