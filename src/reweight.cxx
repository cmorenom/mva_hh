#include <iostream>

#include <TFile.h>
#include <TTree.h>

#include "utils.h"


int main(int argc, char *argv[])
{
	if (argc != 4) {
		std::fprintf(stderr, "usage: reweight <infile-for-weight> <infile-to-reweight> <outfile>\n");
		return 1;
	}

	char *path_for_weight = argv[1];
	char *path_to_reweight = argv[2];
	char *opath = argv[3];

	std::cout << "INFO input for weight: " << path_for_weight << std::endl;
	std::cout << "INFO input to reweight: " << path_to_reweight << std::endl;
	std::cout << "INFO output: " << opath << std::endl;

	TFile in_file_for_weight(path_for_weight, "READ");
	if (in_file_for_weight.IsZombie()) {
		std::cerr << "ERROR: unable to read file " << path_for_weight << std::endl;
		return 1;
	}
	TFile in_file_to_reweight(path_to_reweight, "READ");
	if (in_file_to_reweight.IsZombie()) {
		std::cerr << "ERROR: unable to read file " << path_to_reweight << std::endl;
		return 1;
	}
	TFile ofile(opath, "CREATE");
	if (ofile.IsZombie()) {
		std::cerr << "ERROR: unable to create file " << opath << std::endl;
		return 1;
	}

	for (TKey *key: utils::get_ttree_keys(&in_file_for_weight)) {
		std::cout << "INFO " << key->GetName() << std::endl;

		TTree tree;
		key->Read(&tree);
		tree.SetBranchStatus("*", 0);
		tree.SetBranchStatus("weight_lumi", 1);
		tree.SetBranchStatus("run_number", 1);
		float weight_lumi, weight_lumi_real, acc, acc_real;
                int run_number;
		acc = acc_real = 0;
		tree.SetBranchAddress("weight_lumi", &weight_lumi);
		tree.SetBranchAddress("run_number", &run_number);
		for (Long64_t i = 0; i < tree.GetEntries(); i++) {
			tree.GetEntry(i);
			acc += weight_lumi;
                        weight_lumi_real = weight_lumi*(36207.66*(run_number<324320) + 44307.4*(run_number>=324320 && run_number <348885) + 58450.1*(run_number>=348885));
			acc_real += weight_lumi_real;
		}
		float tgt_weight_lumi = acc;
		float tgt_weight_lumi_real = acc_real;

		TTree *ttree = static_cast<TTree*>(in_file_to_reweight.Get(key->GetName()));
		if (!ttree) {
			std::cerr << "ERROR: tree " << key->GetName() << " not found in " << path_to_reweight << std::endl;
			return 1;
		}
		ttree->SetBranchStatus("*", 0);
		ttree->SetBranchStatus("weight_lumi", 1);
		ttree->SetBranchStatus("run_number", 1);
		ttree->SetBranchAddress("weight_lumi", &weight_lumi);
		ttree->SetBranchAddress("run_number", &run_number);
		acc = acc_real = 0;
		for (Long64_t i = 0; i < ttree->GetEntries(); i++) {
			ttree->GetEntry(i);
			acc += weight_lumi;
                        weight_lumi_real = weight_lumi*(36207.66*(run_number<324320) + 44307.4*(run_number>=324320 && run_number <348885) + 58450.1*(run_number>=348885));
			acc_real += weight_lumi_real;
		}
		float sum_weight_lumi = acc;
		float sum_weight_lumi_real = acc_real;

		ofile.cd();
		ttree->SetBranchStatus("*", 1);
		TTree *newtree = ttree->CloneTree(0);
                newtree->Branch("weight_lumi_real", &weight_lumi_real, "weight_lumi_real/F");
		for (Long64_t i = 0; i < ttree->GetEntries(); i++) {
			ttree->GetEntry(i);
                        weight_lumi_real = weight_lumi*(36207.66*(run_number<324320) + 44307.4*(run_number>=324320 && run_number <348885) + 58450.1*(run_number>=348885));
			weight_lumi *= (tgt_weight_lumi / sum_weight_lumi);
			weight_lumi_real *= (tgt_weight_lumi_real / sum_weight_lumi_real);
			newtree->Fill();
		}
		newtree->Write(0, TObject::kWriteDelete);

	}

	ofile.Close();

	return 0;
}

