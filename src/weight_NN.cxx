#include <cstring>
#include <iostream>
#include <unordered_set>
#include <vector>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

#include <TF2.h>
#include <TFile.h>
#include <TKey.h>
#include <TTree.h>

// Sufficient to identify events from all samples but ttbar
const std::vector<const char*> KEYS = {"did", "event_number"};

// Need more keys for ttbar because of the event numbers in
// filtered-samples problem. See: http://cern.ch/go/B9mD
const std::vector<const char*> KEYS_EXT = {
	"did",
	"event_number",
	"run_number",
	"gen_filt_ht",
	"gen_filt_met",
	"lumiblock_number"
};

// https://nullprogram.com/blog/2018/07/31/
// http://xoshiro.di.unimi.it/splitmix64.c
// https://stackoverflow.com/questions/29855908/c-unordered-set-of-vectors#29855973
template <typename T>
struct Hash64 {
	uint64_t operator()(const std::vector<T>& vec) const {
		uint64_t hash = 0;
		for (T x: vec) {
			hash += static_cast<uint64_t>(x);
			hash ^= hash >> 30;
			hash *= UINT64_C(0xbf58476d1ce4e5b9);
			hash ^= hash >> 27;
			hash *= UINT64_C(0x94d049bb133111eb);
			hash ^= hash >> 31;
		}
		return hash;
	}
};


typedef std::unordered_set<std::vector<double>, Hash64<double>> SetType;


SetType get_set(const char *path, const char *treename)
{
	SetType set;

	TFile tf(path, "READ");
	tf.IsZombie();
	if (tf.IsZombie()) {
		std::cerr << "ERROR: couldn't open the file " << path << std::endl;
		std::exit(EXIT_FAILURE);
	}

	TKey *key = tf.FindKey(treename);
	if (not key) {
		std::cerr << "ERROR: could't find the tree " << treename << std::endl;
		std::exit(EXIT_FAILURE);
	}
	TTree tree;
	key->Read(&tree);

	std::vector<const char*> keys =
		(std::string(treename).find("ttbar") != std::string::npos) ?
		KEYS_EXT :
		KEYS;

	std::vector<double> buf(keys.size());
	tree.SetBranchStatus("*", 0);
	for (size_t i = 0; i < keys.size(); i++) {
		tree.SetBranchStatus(keys.at(i), 1);
		if (tree.SetBranchAddress(keys.at(i), buf.data() + i) < 0) {
			std::cerr << "ERROR: couldn't find the branch "
				  << keys.at(i) << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}

	bool duplicates = false;
	for (Long64_t i = 0; i < tree.GetEntries(); i++) {
		tree.GetEntry(i);
		auto result = set.insert(buf);
		if (not result.second) {
			duplicates = true;
		}
	}

	if (duplicates) {
		std::cerr << "WARNING: duplicates found" << std::endl;
	}

	return set;
}


std::unordered_set<std::string> nominal_trees(const char *path)
{
	TFile tf(path, "READ");
	if (tf.IsZombie()) {
		std::cerr << "ERROR: couldn't open the file " << path << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::unordered_set<std::string> keys;
	TList *lst = tf.GetListOfKeys();
	for (auto it = lst->begin(); it != lst->end(); ++it) {
		TKey *key = static_cast<TKey*>(*it);
		if (std::strcmp(key->GetClassName(), "TTree") == 0 &&
		    std::string(key->GetName()).find("_nominal") != std::string::npos) {
			keys.insert(key->GetName());
		}
	}
	return keys;
}


std::unordered_set<std::string> matching_trees(const std::string &nominal, const char *path)
{
	// e.g. is nominal is Gtt_1200_5000_1_nominal, we want to match Gtt_1200_5000_1_*
	std::string prefix = nominal.substr(0, nominal.find("nominal"));

	TFile tf(path, "READ");
	if (tf.IsZombie()) {
		std::cerr << "ERROR: couldn't open the file " << path << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::unordered_set<std::string> keys;
	TList *lst = tf.GetListOfKeys();
	for (auto it = lst->begin(); it != lst->end(); ++it) {
		TKey *key = static_cast<TKey*>(*it);
		if (std::strcmp(key->GetClassName(), "TTree") == 0 &&
		    std::string(key->GetName()).find(prefix) != std::string::npos) {
			keys.insert(key->GetName());
		}
	}
	return keys;
}


// Normally, events in the test set should not register as inside
// training or validation. This is true for all samples but the ttbar
// one, so this function warns and quantifies the level of "overlap",
// which is the expected amount of lost events because of this.
void warn_test_overlap(const SetType &training_set, const SetType &validation_set,
		       const SetType &test_set)
{
	// test <-> training/validation
	int count = 0;
	for (std::vector<double> x : test_set) {
		if (training_set.count(x) > 0 or validation_set.count(x) > 0) {
			count += 1;
		}
	}
	if (count > 0) {
		double f = 100 * static_cast<double>(count) / test_set.size();
		std::cerr << "WARNING: found " << count << " (" << f << "%)"
			  << " test <-> training/validation ambiguities" << std::endl;
	}
}


// https://stackoverflow.com/questions/1494399/how-do-i-search-find-and-replace-in-a-standard-string
void search_and_replace(std::string& str,
			const std::string& oldStr,
			const std::string& newStr)
{
	std::string::size_type pos = 0u;
	while((pos = str.find(oldStr, pos)) != std::string::npos){
		str.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}
}



// TODO: create a weight for events not in training or validation to
// retain the right overall cross section within this set
void create_variables(const SetType& training_set, const SetType& validation_set, const char *path,
		      const char *treename, bool ext, bool first)
{
	std::unordered_set<std::string> matched = matching_trees(treename, path);

	TFile tf(path, "READ");
	tf.IsZombie();
	if (tf.IsZombie()) {
		std::cerr << "ERROR: couldn't open the file " << path << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::string out = std::string(fs::path(path).stem()) + std::string(".weight_NN.root");
	TFile outfile(out.c_str(), first? "RECREATE" : "UPDATE");

	for (auto it = matched.begin(); it != matched.end(); ++it) {
		std::cerr << "INFO: Output .87 tree: " << *it << std::endl;
		TKey *key = tf.FindKey(it->c_str());
		if (not key) {
			std::cerr << "ERROR: could't find the tree " << *it << std::endl;
			std::exit(EXIT_FAILURE);
		}
		TTree tree;
		key->Read(&tree);

		// Set some defaults here to avoid crossing their initialization
		double scale_factor_den = 0;
		double scale_factor_num = 0;
		double scale_factor_den_aux = 0;
		double scale_factor_num_aux = 0;

		TF2 weight_lumi_real;
		std::string weight_alias;
		// These we really need in all cases:
		std::vector<double> mask(tree.GetEntries(), 1.0);
		std::vector<double> mask_aux(tree.GetEntries(), 1.0);
		double scale_factor = 1;
		double scale_factor_aux = 1;
		if (training_set.empty() and validation_set.empty()) {
			// Skip reading the root file
			std::cerr << "WARNING: no events in training and validation set" << std::endl;
			goto _save_this_tree;
		}

		tree.SetBranchStatus("*", 0);
#define CONNECT(T, b) T b; tree.SetBranchStatus(#b, 1); tree.SetBranchAddress(#b, &b)
		CONNECT(Int_t, did);
		CONNECT(ULong64_t, event_number);
		CONNECT(Int_t, run_number);
		CONNECT(Float_t, gen_filt_ht);
		CONNECT(Float_t, gen_filt_met);
		CONNECT(Int_t, lumiblock_number);
		// Weights
	        CONNECT(Double_t, weight_mc);
		CONNECT(Float_t, weight_lumi);
		CONNECT(Double_t, weight_btag);
		CONNECT(Double_t, weight_elec);
		CONNECT(Double_t, weight_muon);
		CONNECT(Double_t, weight_jvt);
		CONNECT(Double_t, weight_WZ_2_2);
		CONNECT(Double_t, weight_pu);
		// For pre-selection
		CONNECT(Int_t, bjets_n);
		CONNECT(Int_t, signal_leptons_n);
		CONNECT(Float_t, dphi_min);
#undef CONNECT
		// hack for weight_lumi_real
		weight_alias = tree.GetAlias("weight_lumi_real");
		search_and_replace(weight_alias, "weight_lumi", "x");
		search_and_replace(weight_alias, "run_number", "y");
		weight_lumi_real = TF2("weight_lumi_real", weight_alias.c_str());

		// Compute the mask and the scale factor
		for (Long64_t i = 0; i < tree.GetEntries(); i++) {
			tree.GetEntry(i);
			std::vector<double> buf(ext? 6 : 2);
			buf[0] = did;
			buf[1] = event_number;
			if (ext) {
				buf[2] = run_number;
				buf[3] = gen_filt_ht;
				buf[4] = gen_filt_met;
				buf[5] = lumiblock_number;
			}
			bool usable = training_set.count(buf) == 0 and validation_set.count(buf) == 0;

			double weight = weight_mc
				      * weight_btag
				      * weight_elec
				      * weight_muon
				      * weight_jvt
				      * weight_WZ_2_2
                                      * weight_pu
				      * weight_lumi_real.Eval(weight_lumi, run_number);

			if ((bjets_n >= 2)&&((signal_leptons_n >= 1)||(dphi_min >= 0.4))) {
				mask[i] = usable? 1 : 0;
				mask_aux[i] = 0;
				scale_factor_num += weight;
				scale_factor_den += mask[i] * weight;
			} else {
				mask_aux[i] = usable? 1 : 0;
				mask[i] = 0;
				scale_factor_num_aux += weight;
				scale_factor_den_aux += mask_aux[i] * weight;
			}
		}
		if (scale_factor_den != 0)
			scale_factor = scale_factor_num / scale_factor_den;
		if (scale_factor_den_aux != 0)
			scale_factor_aux = scale_factor_num_aux / scale_factor_den_aux;

	_save_this_tree:
		// create the variable
		outfile.cd();
		TTree new_tree(it->c_str(), "");
		double weight_NN = 1;
		new_tree.Branch("weight_NN", &weight_NN);
		for (size_t i = 0; i < mask.size(); i++) {
			if (training_set.empty() and validation_set.empty()) {
				weight_NN = 1;
			} else if (mask[i] and mask_aux[i]) {
				throw std::runtime_error("BOTH MASKS == 1");
			} else {
				weight_NN = mask[i] * scale_factor + mask_aux[i] * scale_factor_aux;
			}
			new_tree.Fill();
		}
		new_tree.Write(0, TObject::kOverwrite);
	}
	outfile.Close();
}


int main(int argc, const char *argv[])
{
	if (argc != 2 && argc != 5) {
		std::cerr << "ERROR: usage: " << argv[0] << "  <.87>"
			  << std::endl;
		std::cerr << "OR" << std::endl;
		std::cerr << "ERROR: usage: " << argv[0] << " <.67-training> <.67-validation> <.67-test> <.87>"
			  << std::endl;
		return EXIT_FAILURE;
	}

	if (argc == 2) {
		const char *master_87 = argv[1];
		SetType dummy;
		create_variables(dummy, dummy, master_87, "", false, true);
		return EXIT_SUCCESS;
	}

	const char *training_67 = argv[1];
	const char *validation_67 = argv[2];
	const char *test_67 = argv[3];
	const char *master_87 = argv[4];



	// Extract sets for all _nominal trees in .67
	// Use each for all matching trees in .87
	// e.g. Gtt_*_5000_*_nominal -> Gtt_<*>_5000_<*>_* (<*> means fixed to same glob as in left term)

	// function: matching_trees(nominal, path)
	std::unordered_set<std::string> trees = nominal_trees(training_67);
	for (auto it = trees.begin(); it != trees.end(); ++it) {
		std::cerr << "INFO: Input .67 tree:" << *it << std::endl;
		std::cerr << "INFO: Training set..." << std::endl;
		SetType training_set = get_set(training_67, it->c_str());
		std::cerr << "INFO: Validation set..." << std::endl;
		SetType validation_set = get_set(validation_67, it->c_str());
		std::cerr << "INFO: Test set..." << std::endl;
		SetType test_set = get_set(test_67, it->c_str());
		std::cerr << "INFO: Checking overlap..." << std::endl;
		warn_test_overlap(training_set, validation_set, test_set);
		std::cerr << "INFO: Creating output variables..." << std::endl;
		create_variables(training_set,
				 validation_set,
				 master_87,
				 it->c_str(),
				 it->find("ttbar") != std::string::npos,
				 it == trees.begin());
	}

}

