// -*- mode: c++; c-basic-offset: 8; -*-
/* select.cxx: Code to select and reformat events from small ntuples
 * produced by the MBJ framework.
 *
 * First, events are pulled from the MBJ ntuples in a "raw"
 * vector-based representation (InData) that reprensents directly the
 * layout of the needed variables in the ntuple. This raw
 * reprensentation is then translated into an intermediate
 * T(Lorentz)Vector based reprensentation (Event) more suitable to
 * physics calculations via the get_event() function. Using this
 * reprensentation, events are selected via the good_event() function
 * and finally transformed to the final, vector-based format (OutData)
 * which maps directly to a flat output format.
 */

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
using namespace std;

#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TLorentzVector.h>
#include <TTree.h>

#include "utils.h"

/* Intermediate representation of an event to make computation on
 * physics objects easier.
 */
struct Event {

	Event(vector<TLorentzVector> &&leptons_,
	      vector<pair<TLorentzVector,bool>> &&jets_,
	      vector<TLorentzVector> &&bjets_,
	      vector<TLorentzVector> &&largejets_,
	      TVector2 &&met_,
	      ULong64_t run_number_,
	      ULong64_t event_number_,
	      Double_t weight_,
	      Double_t met_filter_,
	      Double_t ht_filter_,
	      bool trigger_,
	      Int_t nbaselinelepton_,
	      // Long64_t entry_number_,
	      Float_t weight_lumi_,
	      Float_t weight_lumi_up_,
	      Float_t weight_lumi_down_,
	      Int_t did_,
	      Int_t lumiblock_number_,
	      Int_t ttbar_class_,
	      Int_t top_decay_type_,
	      Int_t antitop_decay_type_,
	      Int_t ttbar_decay_type_,
	      Int_t ttbar_class_ext_,
	      Int_t ttbar_class_prompt_,
	      Double_t weight_ttbar_NNLO_,
	      Double_t weight_ttbar_NNLO_1L_)
		:
		leptons(leptons_),
		jets(jets_),
		bjets(bjets_),
		largejets(largejets_),
		met(met_),
		run_number(run_number_),
		event_number(event_number_),
		weight(weight_),
		met_filter(met_filter_),
		ht_filter(ht_filter_),
		trigger(trigger_),
		nbaselinelepton(nbaselinelepton_),
		// entry_number(entry_number_),
		weight_lumi(weight_lumi_),
		weight_lumi_up(weight_lumi_up_),
		weight_lumi_down(weight_lumi_down_),
		did(did_),
		lumiblock_number(lumiblock_number_),
		ttbar_class(ttbar_class_),
		top_decay_type(top_decay_type_),
		antitop_decay_type(antitop_decay_type_),
		ttbar_decay_type(ttbar_decay_type_),
		ttbar_class_ext(ttbar_class_ext_),
		ttbar_class_prompt(ttbar_class_prompt_),
		weight_ttbar_NNLO(weight_ttbar_NNLO_),
		weight_ttbar_NNLO_1L(weight_ttbar_NNLO_1L_)
		{};

	vector<TLorentzVector> leptons;
	vector<pair<TLorentzVector,bool>> jets;
 	vector<TLorentzVector> bjets;
	vector<TLorentzVector> largejets;

	TVector2 met;
	ULong64_t run_number;
	ULong64_t event_number;
	Double_t weight;
	Double_t met_filter;
	Double_t ht_filter;
	bool trigger;
	Int_t nbaselinelepton;
	// Long64_t entry_number;
	double_t weight_lumi;
	double_t weight_lumi_up;
	double_t weight_lumi_down;
	Int_t did;
	Int_t lumiblock_number;
	Int_t ttbar_class;
	Int_t top_decay_type;
	Int_t antitop_decay_type;
	Int_t ttbar_decay_type;
	Int_t ttbar_class_ext;
	Int_t ttbar_class_prompt;
	Double_t weight_ttbar_NNLO;
	Double_t weight_ttbar_NNLO_1L;

};


/*******************************************************************************
 * Functions to compute observables
 */

Double_t calc_meff(vector<TLorentzVector>& jets,
		 vector<TLorentzVector>& leptons,
		 TVector2& met)
{
	Double_t meff = 0;
	for (TLorentzVector v : jets)
		meff += v.Pt();
	for (TLorentzVector v : leptons)
		meff += v.Pt();
	meff += met.Mod();
	return meff;
}

Double_t calc_mt(vector<TLorentzVector>& leptons, TVector2& met)
{
	if (leptons.size() == 0)
		return 0;

	TLorentzVector lep0 = leptons.at(0);
	return sqrt(2*lep0.Pt()*met.Mod()*(1 - cos(lep0.Phi() - met.Phi())));
}

Double_t calc_mt_min_bjets(vector<TLorentzVector> &bjets, TVector2 &met)
{
	Double_t mt_min = numeric_limits<Double_t>::max();

	for (size_t i = 0; i < bjets.size() && i < 3; i++) {
		TLorentzVector b = bjets.at(i);
		Double_t mt =
			pow(met.Mod() + b.Pt(), 2) -
			pow(met.Px()  + b.Px(), 2) -
			pow(met.Py()  + b.Py(), 2);
		mt = (mt >= 0)? sqrt(mt) : sqrt(-mt);
		if (mt < mt_min)
			mt_min = mt;
	}

	return (bjets.size() > 0)? mt_min : 0;
}

Double_t calc_mjsum(vector<TLorentzVector>& largejets)
{
	Double_t sum = 0;
	for (size_t i = 0; i < 4 && i < largejets.size(); i++)
		sum += largejets.at(i).M();
	return sum;
}

Double_t calc_dphi_min_4j(vector<pair<TLorentzVector,bool>>& jets, TVector2 met)
{
	Double_t min = std::numeric_limits<Double_t>::max();
	for (size_t i = 0; i < 4 && i < jets.size(); i++) {
		TLorentzVector j = jets.at(i).first;
		Double_t dphi = abs(TVector2::Phi_mpi_pi(j.Phi() - met.Phi()));
		if (dphi < min)
			min = dphi;
	}
	return min;
}

Double_t calc_njet(vector<pair<TLorentzVector,bool>>& jets, Double_t ptcut,
		   Double_t absetacut = 2.8)
{
	Double_t njet = 0;

	for (size_t i = 0; i < jets.size(); i++) {
		if ((jets.at(i).first.Pt() > ptcut) &&
		    (abs(jets.at(i).first.Eta()) < absetacut)) {
			njet += 1;
		}
	}

	return njet;
}


Double_t calc_htsoftjets(vector<pair<TLorentzVector,bool>>& jets)
{
	Double_t htsoftjets = 0;

	for (size_t i = 4; i < jets.size(); i++)
		htsoftjets += jets.at(i).first.Pt();

	return htsoftjets;
}

Double_t calc_meffobf(vector<pair<TLorentzVector,bool>>& jets,
		      vector<TLorentzVector>& leptons)
{
	Double_t meffobf = calc_htsoftjets(jets);
	for (size_t i = 0; i < leptons.size(); i++)
		meffobf += leptons.at(i).Pt();
	return (meffobf > 450)? 450 : meffobf;
}

Double_t calc_drlj(vector<pair<TLorentzVector,bool>>& jets,
		      vector<TLorentzVector>& leptons)
{
	Double_t drlj = -1;
	if (leptons.size() > 0) {
		drlj = jets.at(0).first.DeltaR(leptons.at(0));
	}
	return drlj;
}

Double_t calc_drminb(vector<TLorentzVector>& bjets)
{
	Double_t drminb = std::numeric_limits<Double_t>::infinity();

	for (size_t i = 0; i < bjets.size() - 1; i++) {
		for (size_t j = i + 1; j < bjets.size(); j++) {
			Double_t dr = bjets.at(i).DeltaR(bjets.at(j));
			if (dr < drminb)
				drminb = dr;
		}
	}

	return drminb;
}

/*******************************************************************************
 * Helpers for the Event -> OutData conversion
 */


/* Unwrap a vector of TLorentzVector into separate flat vectors */
void fill_output_vectors(std::vector<TLorentzVector>& inputs,
			 std::vector<Double_t>& v1,
			 std::vector<Double_t>& v2,
			 std::vector<Double_t>& v3,
			 std::vector<Double_t>& v4,
			 bool pxpypze)
{
	for(size_t i = 0; i < v1.size(); i++) {
		bool zero = i >= inputs.size();
		if (pxpypze) {
			v1.at(i) = zero ? 0 : inputs.at(i).Px();
			v2.at(i) = zero ? 0 : inputs.at(i).Py();
			v3.at(i) = zero ? 0 : inputs.at(i).Pz();
			v4.at(i) = zero ? 0 : inputs.at(i).E();
		} else {
			v1.at(i) = zero ? 0 : inputs.at(i).Pt();
			v2.at(i) = zero ? 0 : inputs.at(i).Eta();
			v3.at(i) = zero ? 0 : inputs.at(i).Phi();
			v4.at(i) = zero ? 0 : inputs.at(i).M();
		}
	}
}

/* Unwrap a vector of TLorentzVector + tagging information into
 * separate flat vectors
 */
void fill_output_vectors(std::vector<pair<TLorentzVector,bool>>& inputs,
			 std::vector<Double_t>& v1,
			 std::vector<Double_t>& v2,
			 std::vector<Double_t>& v3,
			 std::vector<Double_t>& v4,
			 std::vector<Double_t> &tag,
			 bool pxpypze)
{
	for(size_t i = 0; i < v1.size(); i++) {
		bool zero = i >= inputs.size();
		if (pxpypze) {
			v1.at(i) = zero ? 0 : inputs.at(i).first.Px();
			v2.at(i) = zero ? 0 : inputs.at(i).first.Py();
			v3.at(i) = zero ? 0 : inputs.at(i).first.Pz();
			v4.at(i) = zero ? 0 : inputs.at(i).first.E();
		} else {
			v1.at(i) = zero ? 0 : inputs.at(i).first.Pt();
			v2.at(i) = zero ? 0 : inputs.at(i).first.Eta();
			v3.at(i) = zero ? 0 : inputs.at(i).first.Phi();
			v4.at(i) = zero ? 0 : inputs.at(i).first.M();
		}
		tag.at(i) = zero ? 0 : inputs.at(i).second;
	}
}



/*******************************************************************************
 * Data structure declarations
 */


/* This structure holds data pulled from ROOT files produced by the
 * MBJ framework
 */
struct InData {
	vector<Float_t> *jets_pt;
	vector<Float_t> *jets_eta;
	vector<Float_t> *jets_phi;
	vector<Float_t> *jets_e;
	vector<Int_t> *jets_isb;
	vector<Float_t> *muons_pt;
	vector<Float_t> *muons_eta;
	vector<Float_t> *muons_phi;
	vector<Float_t> *muons_e;
	vector<Float_t> *electrons_pt;
	vector<Float_t> *electrons_eta;
	vector<Float_t> *electrons_phi;
	vector<Float_t> *electrons_e;
  	vector<Float_t> *rc_RCJ_r08pt10_jets_pt;
	vector<Float_t> *rc_RCJ_r08pt10_jets_eta;
	vector<Float_t> *rc_RCJ_r08pt10_jets_phi;
	vector<Float_t> *rc_RCJ_r08pt10_jets_e;
	vector<Float_t> *rc_RCJ_r08pt10_jets_m;
	Float_t mettst;
	Float_t mettst_phi;
	Double_t weight_mc;
	Double_t weight_btag;
	Double_t weight_elec;
	Double_t weight_muon;
    	Double_t weight_pu;
	Double_t weight_jvt;
	Double_t weight_WZ_2_2;
	Double_t weight_ttbar_NNLO;
	Double_t weight_ttbar_NNLO_1L;
	Float_t weight_mettrig;
	ULong64_t run_number;
	ULong64_t event_number;
	Float_t gen_filt_ht;
	Float_t gen_filt_met;
	Int_t lumiblock_number;
        Int_t pass_MET;
	Int_t baselineLep_n;
	// Long64_t entry_number;
	Float_t weight_lumi;
	Float_t weight_lumi_up;
	Float_t weight_lumi_down;
	Int_t did;
	Int_t ttbar_class;
	Int_t top_decay_type;
	Int_t antitop_decay_type;
	Int_t ttbar_decay_type;
	Int_t ttbar_class_ext;
	Int_t ttbar_class_prompt;
};


/* Connect a ttree to an InData structure
 *
 * Arguments:
 *     -- data: the InData structure to be connected
 *     -- chain: the TTree to connect to
 * Returns: Nothing
 */
void connect_indata(InData &data, TTree &chain)
{
	chain.SetBranchStatus("*", 0);
#define CONNECT(b) data.b = 0; chain.SetBranchStatus(#b,1); chain.SetBranchAddress(#b,&(data.b))
	CONNECT(jets_pt);
	CONNECT(jets_eta);
	CONNECT(jets_phi);
	CONNECT(jets_e);
	CONNECT(jets_isb);
	CONNECT(muons_pt);
	CONNECT(muons_eta);
	CONNECT(muons_phi);
	CONNECT(muons_e);
	CONNECT(electrons_pt);
	CONNECT(electrons_eta);
	CONNECT(electrons_phi);
	CONNECT(electrons_e);
	CONNECT(rc_RCJ_r08pt10_jets_pt);
	CONNECT(rc_RCJ_r08pt10_jets_eta);
	CONNECT(rc_RCJ_r08pt10_jets_phi);
	CONNECT(rc_RCJ_r08pt10_jets_e);
	CONNECT(rc_RCJ_r08pt10_jets_m);
	CONNECT(mettst);
	CONNECT(mettst_phi);
	CONNECT(weight_mc);
	CONNECT(weight_btag);
	CONNECT(weight_elec);
	CONNECT(weight_muon);
	CONNECT(weight_pu);
	CONNECT(weight_jvt);
	CONNECT(weight_WZ_2_2);
	CONNECT(weight_ttbar_NNLO);
	CONNECT(weight_ttbar_NNLO_1L);
	CONNECT(run_number);
	CONNECT(event_number);
	CONNECT(gen_filt_ht);
	CONNECT(gen_filt_met);
	CONNECT(pass_MET);
	CONNECT(baselineLep_n);
	// CONNECT(entry_number);
	CONNECT(weight_lumi);
	CONNECT(weight_lumi_up);
	CONNECT(weight_lumi_down);
	CONNECT(did);
	CONNECT(lumiblock_number);
	CONNECT(ttbar_class);
	CONNECT(top_decay_type);
	CONNECT(antitop_decay_type);
	CONNECT(ttbar_decay_type);
	CONNECT(ttbar_class_ext);
	CONNECT(ttbar_class_prompt);
	CONNECT(weight_ttbar_NNLO);
	CONNECT(weight_ttbar_NNLO_1L);

	if (!chain.GetBranch("weight_mettrig")) {
		std::cout << "WARNING tree doesn't have MET trigger scale factors -- substituting all ones\n";
		data.weight_mettrig = 1;
	} else {
		CONNECT(weight_mettrig);
	}

#undef CONNECT

}


/*******************************************************************************
 * OutData
 */

#define CONNECT_I(b,i) do {						\
		std::string key = appendT(#b, i);			\
		tree.Branch(key.c_str(), b.data() + i);			\
	} while (0)
#define CONNECT(b) b = 0; tree.Branch(#b, &b)


/* Helper function to append a string representation of something to a
 * prefix string
 *
 * Arguments:
 *   -- prefix: the prefix string
 *   -- thing: the thing to append
 * Returns:
 *   a string in the "<prefix>_<string repr of `thing`>" format
 */
template <typename T>
std::string appendT(const string& prefix, const T& thing)
{
	std::ostringstream stream;
	stream << prefix << '_' << thing;
	return stream.str();
}


// forward decl
class Event;

/* Structure to hold data in the needed output format. Note that even
 * though vectors are used here to easily make trees with a different
 * about of variables, the output tree will be completely flat.
 */
struct OutData {
	/* N.B. actual inputs are in OutData_PxPyPzE or _PtEtaPhiM */

	/* metadata */
	Double_t weight;
	Double_t event_number;
	Double_t run_number;
	Double_t meff;
	Double_t mt;
	Double_t mtb;
	Double_t mjsum;
	Double_t nb;
	Double_t nlepton;
	Double_t njet30;
	Double_t dphimin4j;
	Double_t met;
	Double_t trigger;
	// Double_t entry_number;
	Double_t weight_lumi;
	Double_t weight_lumi_up;
	Double_t weight_lumi_down;
	Double_t did;
	Double_t weight_lumi_real;
	Double_t gen_filt_met;
	Double_t gen_filt_ht;
	Double_t lumiblock_number;
	Double_t ttbar_class;
	Double_t top_decay_type;
	Double_t antitop_decay_type;
	Double_t ttbar_decay_type;
	Double_t ttbar_class_ext;
	Double_t ttbar_class_prompt;
	Double_t weight_ttbar_NNLO;
	Double_t weight_ttbar_NNLO_1L;

	void connect(TTree &tree);
	void fill(Event &evt);
	virtual void connect_4vec(TTree &tree) = 0;
	virtual void fill_4vec(Event &evt) = 0;
};

/* Connect OutData struct to output TTree
 *
 * Arguments:
 *   -- outdata: The OutData struct to connect
 *   -- tree: The TTree to connect to
 */
void OutData::connect(TTree &tree)
{
	CONNECT(weight);
	CONNECT(event_number);
	CONNECT(run_number);
	CONNECT(meff);
	CONNECT(mt);
	CONNECT(mtb);
	CONNECT(mjsum);
	CONNECT(nb);
	CONNECT(nlepton);
	CONNECT(njet30);
	CONNECT(dphimin4j);
	CONNECT(met);
	CONNECT(trigger);
	// CONNECT(entry_number);
	CONNECT(weight_lumi);
	CONNECT(weight_lumi_up);
	CONNECT(weight_lumi_down);
	CONNECT(did);
	CONNECT(weight_lumi_real);
	CONNECT(gen_filt_met);
	CONNECT(gen_filt_ht);
	CONNECT(lumiblock_number);
	CONNECT(ttbar_class);
	CONNECT(top_decay_type);
	CONNECT(antitop_decay_type);
	CONNECT(ttbar_decay_type);
	CONNECT(ttbar_class_ext);
	CONNECT(ttbar_class_prompt);
	CONNECT(weight_ttbar_NNLO);
	CONNECT(weight_ttbar_NNLO_1L);

	connect_4vec(tree);
}

/* Event -> OutData conversion
 *
 * Arguments:
 *   -- event: The Event structure to convert
 *   -- outdata: The OutData struct to convert into
 *   -- scale: quantity by which to scale the event weight.
 *             can be used to scale the sum of the weights to the
 *             desired xsec.
 * Returns:
 *   Nothing
 */
void OutData::fill(Event &evt)
{
	vector<TLorentzVector> jets_tlv_only;
	for (auto p : evt.jets)
		jets_tlv_only.push_back(p.first);

	weight = evt.weight;
	event_number = evt.event_number;
	run_number = evt.run_number;
	meff = calc_meff(jets_tlv_only, evt.leptons, evt.met);
	mt = calc_mt(evt.leptons, evt.met);
	mtb = calc_mt_min_bjets(evt.bjets, evt.met);
	mjsum = calc_mjsum(evt.largejets);
	nb = evt.bjets.size();
	nlepton = evt.leptons.size();
	njet30 = calc_njet(evt.jets, 30);
	dphimin4j = calc_dphi_min_4j(evt.jets, evt.met);
	met = evt.met.Mod();
	trigger = evt.trigger;
	// entry_number = evt.entry_number;
	weight_lumi = evt.weight_lumi;
	weight_lumi_up = evt.weight_lumi_up;
	weight_lumi_down = evt.weight_lumi_down;
	did = evt.did;
	gen_filt_met = evt.met_filter;
	gen_filt_ht = evt.ht_filter;
	lumiblock_number = evt.lumiblock_number;
	ttbar_class = evt.ttbar_class;
	top_decay_type = evt.top_decay_type;
	antitop_decay_type = evt.antitop_decay_type;
	ttbar_decay_type = evt.ttbar_decay_type;
	ttbar_class_ext = evt.ttbar_class_ext;
	ttbar_class_prompt = evt.ttbar_class_prompt;
	weight_ttbar_NNLO = evt.weight_ttbar_NNLO;
	weight_ttbar_NNLO_1L = evt.weight_ttbar_NNLO_1L;

	double lumi;
	if (run_number < 324320)
		lumi = 36207.66;  // 2015+2016
	else if (run_number < 348885)
		lumi = 44307.40;  // 2017
	else
		lumi = 58450.10;  // 2018

	weight_lumi_real = weight_lumi * lumi;

	fill_4vec(evt);
}


struct OutData_PxPyPzE : public OutData {
public:
	std::vector<Double_t> small_R_jets_px;
	std::vector<Double_t> small_R_jets_py;
	std::vector<Double_t> small_R_jets_pz;
	std::vector<Double_t> small_R_jets_e;
	std::vector<Double_t> small_R_jets_isb;
	std::vector<Double_t> large_R_jets_px;
	std::vector<Double_t> large_R_jets_py;
	std::vector<Double_t> large_R_jets_pz;
	std::vector<Double_t> large_R_jets_e;
	std::vector<Double_t> leptons_px;
	std::vector<Double_t> leptons_py;
	std::vector<Double_t> leptons_pz;
	std::vector<Double_t> leptons_e;
	Double_t met_px;
	Double_t met_py;

	OutData_PxPyPzE(int n_small, int n_large, int n_lepton);
	void connect_4vec(TTree &tree);
	void fill_4vec(Event &evt);
};

/* Constructor that initialize all vectors to requested size
 *
 * Arguments:
 *   -- n_small: number of small-R jets
 *   -- n_large: number of large-R jets
 *   -- n_lepton: number of leptons
 * Returns:
 *   an empty OutData structure
 */
OutData_PxPyPzE::OutData_PxPyPzE(int n_small, int n_large, int n_lepton)
{
	small_R_jets_px.resize(n_small);
	small_R_jets_py.resize(n_small);
	small_R_jets_pz.resize(n_small);
	small_R_jets_e.resize(n_small);
	small_R_jets_isb.resize(n_small);

	large_R_jets_px.resize(n_large);
	large_R_jets_py.resize(n_large);
	large_R_jets_pz.resize(n_large);
	large_R_jets_e.resize(n_large);

	leptons_px.resize(n_lepton);
	leptons_py.resize(n_lepton);
	leptons_pz.resize(n_lepton);
	leptons_e.resize(n_lepton);
}

void OutData_PxPyPzE::connect_4vec(TTree &tree)
{
	for (size_t i = 0; i < small_R_jets_px.size(); i++) {
		CONNECT_I(small_R_jets_px, i);
		CONNECT_I(small_R_jets_py, i);
		CONNECT_I(small_R_jets_pz, i);
		CONNECT_I(small_R_jets_e, i);
		CONNECT_I(small_R_jets_isb, i);
	}
	for (size_t i = 0; i < large_R_jets_px.size(); i++) {
		CONNECT_I(large_R_jets_px, i);
		CONNECT_I(large_R_jets_py, i);
		CONNECT_I(large_R_jets_pz, i);
		CONNECT_I(large_R_jets_e, i);
	}
	for (size_t i = 0; i < leptons_px.size(); i++) {
		CONNECT_I(leptons_px, i);
		CONNECT_I(leptons_py, i);
		CONNECT_I(leptons_pz, i);
		CONNECT_I(leptons_e, i);
	}

	CONNECT(met_px);
	CONNECT(met_py);
}

void OutData_PxPyPzE::fill_4vec(Event &evt)
{
	fill_output_vectors(evt.jets,
			    small_R_jets_px,
			    small_R_jets_py,
			    small_R_jets_pz,
			    small_R_jets_e,
			    small_R_jets_isb,
			    true
		);

	fill_output_vectors(evt.largejets,
			    large_R_jets_px,
			    large_R_jets_py,
			    large_R_jets_pz,
			    large_R_jets_e,
			    true
		);

	fill_output_vectors(evt.leptons,
			    leptons_px,
			    leptons_py,
			    leptons_pz,
			    leptons_e,
			    true
		);

	met_px = evt.met.Px();
	met_py = evt.met.Py();
}

struct OutData_PtEtaPhiM : public OutData {
public:
	std::vector<Double_t> small_R_jets_pt;
	std::vector<Double_t> small_R_jets_eta;
	std::vector<Double_t> small_R_jets_phi;
	std::vector<Double_t> small_R_jets_m;
	std::vector<Double_t> small_R_jets_isb;
	std::vector<Double_t> large_R_jets_pt;
	std::vector<Double_t> large_R_jets_eta;
	std::vector<Double_t> large_R_jets_phi;
	std::vector<Double_t> large_R_jets_m;
	std::vector<Double_t> leptons_pt;
	std::vector<Double_t> leptons_eta;
	std::vector<Double_t> leptons_phi;
	std::vector<Double_t> leptons_m;
	Double_t met_mod;
	Double_t met_phi;

	OutData_PtEtaPhiM(int n_small, int n_large, int n_lepton);
	void connect_4vec(TTree &tree);
	void fill_4vec(Event &evt);
};

/* Constructor that initialize all vectors to requested size
 *
 * Arguments:
 *   -- n_small: number of small-R jets
 *   -- n_large: number of large-R jets
 *   -- n_lepton: number of leptons
 * Returns:
 *   an empty OutData structure
 */
OutData_PtEtaPhiM::OutData_PtEtaPhiM(int n_small, int n_large, int n_lepton)
{
	small_R_jets_pt.resize(n_small);
	small_R_jets_eta.resize(n_small);
	small_R_jets_phi.resize(n_small);
	small_R_jets_m.resize(n_small);
	small_R_jets_isb.resize(n_small);

	large_R_jets_pt.resize(n_large);
	large_R_jets_eta.resize(n_large);
	large_R_jets_phi.resize(n_large);
	large_R_jets_m.resize(n_large);

	leptons_pt.resize(n_lepton);
	leptons_eta.resize(n_lepton);
	leptons_phi.resize(n_lepton);
	leptons_m.resize(n_lepton);
}

void OutData_PtEtaPhiM::connect_4vec(TTree &tree)
{
	for (size_t i = 0; i < small_R_jets_pt.size(); i++) {
		CONNECT_I(small_R_jets_pt, i);
		CONNECT_I(small_R_jets_eta, i);
		CONNECT_I(small_R_jets_phi, i);
		CONNECT_I(small_R_jets_m, i);
		CONNECT_I(small_R_jets_isb, i);
	}
	for (size_t i = 0; i < large_R_jets_pt.size(); i++) {
		CONNECT_I(large_R_jets_pt, i);
		CONNECT_I(large_R_jets_eta, i);
		CONNECT_I(large_R_jets_phi, i);
		CONNECT_I(large_R_jets_m, i);
	}
	for (size_t i = 0; i < leptons_pt.size(); i++) {
		CONNECT_I(leptons_pt, i);
		CONNECT_I(leptons_eta, i);
		CONNECT_I(leptons_phi, i);
		CONNECT_I(leptons_m, i);
	}

	CONNECT(met_mod);
	CONNECT(met_phi);
}

void OutData_PtEtaPhiM::fill_4vec(Event &evt)
{
	fill_output_vectors(evt.jets,
			    small_R_jets_pt,
			    small_R_jets_eta,
			    small_R_jets_phi,
			    small_R_jets_m,
			    small_R_jets_isb,
			    false
		);

	fill_output_vectors(evt.largejets,
			    large_R_jets_pt,
			    large_R_jets_eta,
			    large_R_jets_phi,
			    large_R_jets_m,
			    false
		);

	fill_output_vectors(evt.leptons,
			    leptons_pt,
			    leptons_eta,
			    leptons_phi,
			    leptons_m,
			    false
		);

	met_mod = evt.met.Mod();
	met_phi = evt.met.Phi();
}



#undef CONNECT_I
#undef CONNECT

/*******************************************************************************
 * TLorentzVector helper functions
 */


/* Make a TLorentzVector in one call from pt, eta, phi and energy */
TLorentzVector make_tlv(Double_t pt, Double_t eta, Double_t
phi, Double_t e) { TLorentzVector tlv; tlv.SetPtEtaPhiE(pt,eta,phi,e);
return tlv; }


/* Used to sort containers of TLorentzVector by decreasing pT */
bool compare_tlv(TLorentzVector v1, TLorentzVector v2)
{
	return v1.Pt() > v2.Pt();
}


/* Used to sort containers of TLorentzVector by decreasing pT, when
 * tagging information is included
 */
bool compare_tlv_in_pair(pair<TLorentzVector,bool> a,
			 pair<TLorentzVector,bool> b)
{
	return compare_tlv(a.first, b.first);
}


/*******************************************************************************
 * Helpers for the InData -> Event conversion
 */

vector<TLorentzVector> get_leptons(InData& data)
{
	vector<TLorentzVector> leptons;

	for (size_t i = 0; i < data.electrons_pt->size(); ++i) {
	        Double_t pt = data.electrons_pt->at(i);
		Double_t eta = data.electrons_eta->at(i);
		Double_t phi = data.electrons_phi->at(i);
		Double_t e = data.electrons_e->at(i);
		leptons.push_back(make_tlv(pt,eta,phi,e));
	}

	for (size_t i = 0; i < data.muons_pt->size(); ++i) {
	        Double_t pt = data.muons_pt->at(i);
		Double_t eta = data.muons_eta->at(i);
		Double_t phi = data.muons_phi->at(i);
		Double_t e = data.muons_e->at(i);
		leptons.push_back(make_tlv(pt,eta,phi,e));
	}

	sort(leptons.begin(),leptons.end(),compare_tlv);

	return leptons;
}

vector<pair<TLorentzVector,bool>> get_jets(InData &data)
{
	vector<pair<TLorentzVector,bool> > jets;

	for (size_t i = 0; i < data.jets_pt->size(); i++) {
		Double_t pt = data.jets_pt->at(i);
		Double_t eta = data.jets_eta->at(i);
		Double_t phi = data.jets_phi->at(i);
		Double_t e = data.jets_e->at(i);
		Double_t isb = data.jets_isb->at(i) && pt > 30 && abs(eta) < 2.5;
		if (pt > 30 && abs(eta) < 2.8) {
			pair<TLorentzVector,bool> pair(
				make_tlv(pt,eta,phi,e),
				isb);
			jets.push_back(move(pair));
		}
	}

	sort(jets.begin(), jets.end(), compare_tlv_in_pair);

	return jets;
}


vector<TLorentzVector> get_bjets(InData &data)
{
	vector<TLorentzVector> jets;

	for (size_t i = 0; i < data.jets_pt->size(); i++) {
		Double_t pt = data.jets_pt->at(i);
		Double_t eta = data.jets_eta->at(i);
		Double_t phi = data.jets_phi->at(i);
		Double_t e = data.jets_e->at(i);
		bool isb = data.jets_isb->at(i) == 1;

		if (pt > 30 && abs(eta) < 2.5 && isb)
			jets.push_back(make_tlv(pt,eta,phi,e));
	}

	sort(jets.begin(), jets.end(), compare_tlv);

	return jets;
}


vector<TLorentzVector> get_largeR_jets(InData &data)
{
	vector<TLorentzVector> jets;

	for (size_t i = 0; i < data.rc_RCJ_r08pt10_jets_pt->size(); i++) {
		Double_t pt = data.rc_RCJ_r08pt10_jets_pt->at(i);
		Double_t eta = data.rc_RCJ_r08pt10_jets_eta->at(i);
		Double_t phi = data.rc_RCJ_r08pt10_jets_phi->at(i);
		Double_t e = data.rc_RCJ_r08pt10_jets_e->at(i);
		if (pt > 100 && abs(eta) < 2.0)
			jets.push_back(make_tlv(pt,eta,phi,e));
	}

	sort(jets.begin(), jets.end(), compare_tlv);

	return jets;
}

TVector2 get_met(InData &data)
{
	TVector2 v;
	v.SetMagPhi(data.mettst, data.mettst_phi);
	return v;
}


/* put everything together */
Event get_event(InData& data, bool isdata)
{
	Double_t weight = 1;

	if (!isdata) {
		weight *=
			data.weight_mc *
			data.weight_btag *
			data.weight_elec *
			data.weight_muon *
			data.weight_pu *
			data.weight_jvt *
			data.weight_WZ_2_2 *
			data.weight_mettrig;
	}

	Event evt(get_leptons(data),
		  get_jets(data),
		  get_bjets(data),
		  get_largeR_jets(data),
		  get_met(data),
		  data.run_number,
		  data.event_number,
		  weight,
		  data.gen_filt_met,
		  data.gen_filt_ht,
		  data.pass_MET,
		  data.baselineLep_n,
		  // data.entry_number,
		  data.weight_lumi,
		  data.weight_lumi_up,
		  data.weight_lumi_down,
		  data.did,
		  data.lumiblock_number,
		  data.ttbar_class,
		  data.top_decay_type,
		  data.antitop_decay_type,
		  data.ttbar_decay_type,
		  data.ttbar_class_ext,
		  data.ttbar_class_prompt,
		  data.weight_ttbar_NNLO,
		  data.weight_ttbar_NNLO_1L);
	return evt;
}


/* Decide if good event or not, with met and ht cuts */
bool good_event(Event &event, TH1D &cutflow, TH1D &cutflow_w)
{
	cutflow.Fill(0);
	cutflow_w.Fill(0., event.weight);
	if (!event.trigger)
		return false;
	cutflow.Fill(1);
	cutflow_w.Fill(1, event.weight);
	if (event.met.Mod() < 100)
		return false;
	cutflow.Fill(2);
	cutflow_w.Fill(2, event.weight);
	if (event.jets.size() < 4)
		return false;
	cutflow.Fill(3);
	cutflow_w.Fill(3, event.weight);
	if (event.bjets.size() < 2)
		return false;
	cutflow.Fill(4);
	cutflow_w.Fill(4, event.weight);
	if (event.leptons.size() == 0 &&
	    calc_dphi_min_4j(event.jets, event.met) < 0.4)
		return false;
	cutflow.Fill(5);
	cutflow_w.Fill(5, event.weight);

	return true;
}

void make_tree(TKey *key, int nsmall, int nlarge, int nlepton, bool allpass,
	       bool pxpypze, bool isdata, TFile &outfile)
{
	std::cout << "INFO making NN input for tree: " << key->GetName() << '\n';
	TTree intree;
	key->Read(&intree);
	outfile.cd();

	TH1D h_cutflow("cutflow", "", 8, 0, 8);
	TH1D h_cutflow_w("cutflow_weighted", "", 8, 0, 8);
	h_cutflow_w.Sumw2();

	InData indata;
	connect_indata(indata, intree);

	TTree outtree(key->GetName(), "");

	OutData *outdata;
	if (pxpypze)
		outdata = new OutData_PxPyPzE(nsmall, nlarge, nlepton);
	else
		outdata = new OutData_PtEtaPhiM(nsmall, nlarge, nlepton);
	outdata->connect(outtree);

	for (Long64_t i = 0; i < intree.GetEntries(); ++i) {
		intree.GetEntry(i);
		Event evt = get_event(indata, isdata);
		if (allpass || good_event(evt, h_cutflow, h_cutflow_w)) {
			outdata->fill(evt);
			outtree.Fill();
		}
	}

	outtree.Write(0, TObject::kWriteDelete);
}

// usage: makeNNinput input output nsmall nlarge nlepton allpass pxpypze isdata
//        ^0          ^1    ^2     ^3     ^4     ^5      ^6      ^7      ^8
int main(int argc, char *argv[])
{

	if (argc != 9) {
		fprintf(stderr, "ERROR: too few arguments\n");
		fprintf(stderr, "usage: makeNNinput input output nsmall nlarge nlepton allpass pxpypze isdata\n");
		return 1;
	}

	std::cout << "INFO input: " << argv[1] << '\n';
	std::cout << "INFO output: " << argv[2] << '\n';
	std::cout << "INFO nsmall: " << argv[3] << '\n';
	std::cout << "INFO nlarge: " << argv[4] << '\n';
	std::cout << "INFO nlepton: " << argv[5] << '\n';
	std::cout << "INFO allpass: " << argv[6] << '\n';
	std::cout << "INFO pxpypze: " << argv[7] << '\n';
	std::cout << "INFO isdata: " << argv[8] << '\n';

	char *input_path = argv[1];
	char *output_path = argv[2];
	int nsmall = atoi(argv[3]);
	int nlarge = atoi(argv[4]);
	int nlepton = atoi(argv[5]);
	bool allpass = atoi(argv[6]);
	bool pxpypze = atoi(argv[7]);
	bool isdata = atoi(argv[8]);

	TFile *infile = TFile::Open(input_path, "READ");

	TFile outfile(output_path, "CREATE");
	if (outfile.IsZombie()) {
		fprintf(stderr, "ERROR: unable to open output file %s\n", output_path);
		return 1;
	}

	for (TKey *key : utils::get_ttree_keys(infile))
		make_tree(key, nsmall, nlarge, nlepton, allpass, pxpypze, isdata, outfile);

	return 0;
}
