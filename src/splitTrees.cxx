#include <array>
#include <cstring>
#include <iostream>
#include <map>
#include <random>
#include <string>

#include <TFile.h>
#include <TKey.h>
#include <TTree.h>

#include "utils.h"

std::map<std::string, std::vector<TKey*>> get_keys_by_tree(TFile *tf)
{
	std::map<std::string, std::vector<TKey*>> treemap;
	for (TKey *key : utils::get_ttree_keys(tf)) {
		std::string name(key->GetName());
		size_t i = name.find_last_of("_");
		std::string treename = name.substr(0, i);
		treemap[treename].push_back(key);
	}
	return treemap;
}


int main(int argc, char *argv[])
{
	std::cout << "INFO command=\"";
	for (int i = 0; i < argc; i++)
		std::cout << argv[i] << ((i < (argc - 1))? " " : "\"\n");

	if (!(argc == 5 || argc == 6)) {
		std::cout << "ERROR: usage: splitTrees <path> <train_frac> <valid_frac> <test_frac> [seed]\n";
		return EXIT_FAILURE;
	}

	const char *input_path = argv[1];
	std::cout << "INFO input=" << input_path << '\n';

	std::array<double, 3> weights;
	for (size_t i = 0; i < 3; i++) {
		try {
			weights[i] = std::stod(argv[i + 2]);
		} catch (const std::invalid_argument&) {
			std::cerr << "ERROR: invalid fraction: "
				  << argv[i + 2] << '\n';
			return EXIT_FAILURE;
		}
	}
	std::cout << "INFO weights={" << weights[0] << ", " << weights[1] << ", " << weights[2] << "}\n";

	unsigned int seed;
	if (argc == 6) {
		try {
			seed = std::stoul(argv[5]);
		} catch (const std::invalid_argument&) {
			std::cerr << "ERROR: invalid seed: "
				  << argv[5] << '\n';
			return EXIT_FAILURE;
		}
	} else {
		std::random_device rd;
		seed = rd();
	}

	std::cout << "INFO seed=" << seed << '\n';

	std::mt19937 rng;
	rng.seed(seed);
	std::discrete_distribution<int> idist(weights.begin(), weights.end());

	TFile *input_file = TFile::Open(input_path, "READ");
	if (!input_file || input_file->IsZombie())
		return EXIT_FAILURE;

	std::array<TFile*,3> outputs;
	std::string prefix = utils::basename(input_path);
	std::cout << "INFO output_training=" << (prefix + ".training") << '\n';
	if (!(outputs[0] = TFile::Open((prefix + ".training").c_str(), "CREATE")))
		return EXIT_FAILURE;
	std::cout << "INFO output_validation=" << (prefix + ".validation") << '\n';
	if (!(outputs[1] = TFile::Open((prefix + ".validation").c_str(), "CREATE")))
		return EXIT_FAILURE;
	std::cout << "INFO output_test=" << (prefix + ".test") << '\n';
	if (!(outputs[2] = TFile::Open((prefix + ".test").c_str(), "CREATE")))
		return EXIT_FAILURE;

	// loop over tree "type" (all different variation of single tree)
	std::map<std::string, std::vector<TKey*>> keys = get_keys_by_tree(input_file);
	for (auto it = keys.begin(); it != keys.end(); ++it) {
		std::cout << "INFO prefix=" << it->first << std::endl;
		// loop over trees with same prefix
		bool first = true;
		std::vector<int> indices;
		for (TKey *key : it->second) {
			std::cout << "INFO treename=" << key->GetName() << std::endl;
			TTree input_tree;
			std::array<TTree*, 3> output_trees;
			key->Read(&input_tree);
			Long64_t ievt;
			for (size_t i = 0; i < 3; i++) {
				outputs[i]->cd();
				output_trees[i] = input_tree.CloneTree(0);
				output_trees[i]->Branch("entry_number", &ievt);
			}
			// loop over events
			for (ievt = 0; ievt < input_tree.GetEntries(); ievt++) {
				input_tree.GetEntry(ievt);
				// pick output fold
				int itree;
				if (first) {
					itree = idist(rng);
					indices.push_back(itree);
				} else {
					itree = indices.at(ievt);
				}
				outputs[itree]->cd();
				output_trees[itree]->Fill();
			}
			for (size_t i = 0; i < 3; i++) {
				outputs[i]->cd();
				output_trees[i]->Write(0, TObject::kWriteDelete);
			}
			first = false;
		}
	}

	for (size_t i = 0; i < 3; i++)
		outputs[i]->Close();
	input_file->Close();

	return 0;
}
