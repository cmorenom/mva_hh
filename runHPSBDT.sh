
i=0

for numTrees in 100 #350 500 650
do
    for eta in 0.15 0.3 0.5
    do 
	for subsample in 0.6 0.8
	do 
	    for maxBin in 25 50
	    do 
		for maxDepth in 3 4 5 6
		do
		    echo $i
		    echo 'Creating model'
		    python ./models/generators/generator_BDT.py --outputName 'config/models/1100/BDT_'$i'.json' --numTrees $numTrees --eta $eta --subsample $subsample --maxBin $maxBin --maxDepth $maxDepth
		    echo 'Model '$i' created successfully. Running BDT now'
		    nohup python3 ./scripts/fullBDTChain.py --files ../samples/training/*.training ../samples/test/*.test --sel config/sel.json --sel-sig config/sel_sig.json --branches config/branches_highlevel.list --group-to-target config/group_to_target_1100.json --model config/models/1100/BDT_$i.json --num-rounds 150 --evBkg 20000 --storeBDT --output models/HPS/1100/BDT_$i --plotname plots/HPS/1100/BDT_$i &
		    echo 'BDT training and evaluation processing in background. Next model'

		    ((i=i+1))
		done
	    done
	done
    done
done

