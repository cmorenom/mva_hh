import json
import uuid

import numpy as np

from mbj_nn import utils

log_lr = np.log10(1.582039518153811e-05)

hyperparameters = {
    'n_hidden_layers': 3,
    'n_hidden_units': 737,
    'learning_rate': 10 ** np.random.uniform(log_lr - 1, log_lr + 1),
    'batch_size': 128,
    'dropout_input': 0.0,
    'dropout_hidden': 0.0,
    'batchnorm': True,
    'hidden_l1': 1.5685986136426562e-05,
    'hidden_l2': 2.305601121163932e-07,
    'output_l1': 0.0,
    'output_l2': 0.0,
    'earlystopping_fscore': False,
    'use_weights': True,
    'name': 'generator_2.387aee92-LR.' + str(uuid.uuid4()),
}

with open(hyperparameters['name'] + '.json', 'w') as ofile:
    json.dump(hyperparameters, ofile, indent=4)

print('%s.json' % hyperparameters['name'])
