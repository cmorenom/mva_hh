import json
import uuid

import numpy as np

from mbj_nn import utils

hyperparameters = {
    'n_hidden_layers': np.random.randint(1, 5),
    'n_hidden_units': np.random.randint(50, 1001),
    'learning_rate': utils.draw_exponentially(1e-4, 1e-2),
    'batch_size': 2 ** np.random.randint(5, 10),  # (32, 512)
    'dropout_input': np.random.choice([0.0, 0.2]),
    'dropout_hidden': np.random.choice([0.0, 0.5]),
    'batchnorm': bool(np.random.choice([True, False])),
    'hidden_l1': np.random.choice([0, utils.draw_exponentially(1e-7, 1e-4)]),
    'hidden_l2': np.random.choice([0, utils.draw_exponentially(1e-7, 1e-4)]),
    'output_l1': np.random.choice([0, utils.draw_exponentially(1e-7, 1e-4)]),
    'output_l2': np.random.choice([0, utils.draw_exponentially(1e-7, 1e-4)]),
    'earlystopping_fscore': bool(np.random.choice([True, False])),
    'use_weights': bool(np.random.choice([True, False])),
    'name': 'generator_1.' + str(uuid.uuid4()),
}

with open(hyperparameters['name'] + '.json', 'w') as ofile:
    json.dump(hyperparameters, ofile, indent=4)

print '%s.json' % hyperparameters['name']
