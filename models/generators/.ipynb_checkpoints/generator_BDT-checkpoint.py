import json
import argparse

import numpy as np

from mbj_nn import utils

args = argparse.ArgumentParser()
args.add_argument('--outputName', type=str, required = True)
args.add_argument('--numTrees', type=int, default = 200)
args.add_argument('--eta', type=float, default = 0.3)
args.add_argument('--subsample', type = float, default = 0.6)
args.add_argument('--maxBin', type = int, default = 20)
args.add_argument('--maxDepth', type = int, default = 3)

parseargs = args.parse_args()

hyperparameters = {
    'num_parallel_tree': parseargs.numTrees,
    'tree_method': 'hist',
    'booster': 'gbtree',
    'eta': parseargs.eta,
    'subsample': parseargs.subsample,
    'max_bin': parseargs.maxBin,
    'max_depth': parseargs.maxDepth,
    'objective': 'binary:logistic'
}

with open(parseargs.outputName, 'w') as ofile:
    json.dump(hyperparameters, ofile, indent=4)
    
