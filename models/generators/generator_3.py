import json
import uuid

import numpy as np

from mbj_nn import utils

hyperparameters = {
    'n_hidden_layers': np.random.randint(1, 6),
    'n_hidden_units': np.random.randint(200, 1001),
    'learning_rate': 10 ** np.random.uniform(-6, -2),
    'batch_size': 2 ** np.random.randint(5, 10),  # (32, 512)
    'dropout_input': np.random.choice([0.0, 0.2]),
    'dropout_hidden': np.random.choice([0.0, 0.5]),
    'batchnorm': bool(np.random.choice([True, False])),
    'hidden_l1': np.random.choice([0, 10 ** np.random.uniform(-7, -2)]),
    'hidden_l2': np.random.choice([0, 10 ** np.random.uniform(-7, -2)]),
    'output_l1': np.random.choice([0, 10 ** np.random.uniform(-7, -2)]),
    'output_l2': np.random.choice([0, 10 ** np.random.uniform(-7, -2)]),
    'earlystopping_fscore': bool(np.random.choice([True, False])),
    'use_weights': True,
    'name': 'generator_3.' + str(uuid.uuid4()),
}

if hyperparameters['n_hidden_layers']:
    hyperparameters['batchnorm'] == False

with open(hyperparameters['name'] + '.json', 'w') as ofile:
    json.dump(hyperparameters, ofile, indent=4)

print('%s.json' % hyperparameters['name'])
