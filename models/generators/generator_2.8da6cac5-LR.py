import json
import uuid

import numpy as np

from mbj_nn import utils

log_lr = np.log10(0.0013252631630748144)

hyperparameters = {
    'n_hidden_layers': 3,
    'n_hidden_units': 333,
    'learning_rate': 10 ** np.random.uniform(log_lr - 1, log_lr + 1),
    'batch_size': 128,
    'dropout_input': 0.2,
    'dropout_hidden': 0.5,
    'batchnorm': True,
    'hidden_l1': 0.0,
    'hidden_l2': 0.0,
    'output_l1': 0.0005914405688477145,
    'output_l2': 0.0,
    'earlystopping_fscore': True,
    'use_weights': True,
    'name': 'generator_2.8da6cac5-LR.' + str(uuid.uuid4()),
}

with open(hyperparameters['name'] + '.json', 'w') as ofile:
    json.dump(hyperparameters, ofile, indent=4)

print('%s.json' % hyperparameters['name'])
